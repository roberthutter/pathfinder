document.addEventListener("DOMContentLoaded", function () {
  const pillDiv = document.getElementById("pillDiv");
  const car = document.createElement("div");
  car.className = "car";
  pillDiv.appendChild(car);

  function moveCar() {
    const pillWidth = pillDiv.offsetWidth;
    const carWidth = car.offsetWidth;

    car.style.transition = "none";
    car.style.transform = `translate(-${carWidth}px, -50%)`; // Move the car outside the div to the left
    pillDiv.offsetWidth;
    car.style.transition = "transform 1s linear"; // Faster transition, adjust as needed

    requestAnimationFrame(() => {
      car.style.transform = `translate(${pillWidth}px, -50%)`; // Move the car outside the div to the right
    });
  }

  function resetCar() {
    car.style.transition = "none";
    car.style.transform = "translate(-100%, -50%)";
    pillDiv.offsetWidth;
    car.style.transition = "transform 0s linear"; // Reset transition duration to 0s
  }

  pillDiv.addEventListener("click", function () {
    const numIterations = 10;
    let iterationCount = 0;

    function doIteration() {
      resetCar(); // Reset car position before starting the animation
      moveCar(); // Start the animation

      iterationCount++;

      if (iterationCount < numIterations) {
        // Set a timeout to repeat the animation after the same duration as the animation
        setTimeout(doIteration, 1000); // 1 second, adjust as needed
      }
    }

    // Start the loop
    doIteration();
  });
});
