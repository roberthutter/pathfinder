//author: Luca Schütt
//version: 21.12.23

//--------------------------------------------------------//
//-----------------------PAGE SETUP-----------------------//
//--------------------------------------------------------//

// Initializing the map with custom zoom control on page load.
const map = L.map("map", {
  center: [48.74583850063722, 9.105412264957042],
  zoom: 18,
  zoomControl: false,
  attributionControl: true,
});

L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    "© OpenStreetMap contributors | <a href='/info.html' target='_blank'>About Pathfinder</a>",
}).addTo(map);
L.control.zoom({ position: "topright" }).addTo(map);

// Clear forms and disable buttons
let markers = [];
const calculateButton = document.getElementById("calculateButton");
clearAllFormInputs();
updateCalculateButtonState();
updateClearButtonState();
updateMenuItem4State();

/**
 * Clears the contents of all forms on the page.
 */
function clearAllFormInputs() {
  document.querySelectorAll("form").forEach((form) => {
    form.reset();
  });
}

/**
 * Updates the state of the calculateButton based on the number of markers.
 */
function updateCalculateButtonState() {
  calculateButton.disabled = markers.length !== 2;
  updateMenuItem4State();
}

/**
 * Updates the state of the calculateButton based on the number of markers.
 */
function updateClearButtonState() {
  clearButton.disabled = markers.length == 0;
}

/**
 * Updates the "Calculate" option on the context menu.
 */
function updateMenuItem4State() {
  if (calculateButton.disabled) {
    menuItem4.removeEventListener("click", menuItem4ClickHandler);
    menuItem4.classList.add("menu-item-disabled");
  } else {
    menuItem4.addEventListener("click", menuItem4ClickHandler);
    menuItem4.classList.remove("menu-item-disabled");
  }
}

//--------------------------------------------------------//
//---------------Custom Marker Declarations---------------//
//--------------------------------------------------------//

// custom start marker icon
const startIcon = L.icon({
  iconUrl: "/images/startMarker.png",
  iconSize: [64, 64], // Size of the icon [width, height]
  iconAnchor: [32, 64], // Point of the icon which will correspond to marker's location [half of width, height]
  shadowUrl: "/images/startMarkerShadow.png", // URL to the shadow image
  shadowSize: [64, 64], // size of the shadow
  shadowAnchor: [18, 64], // the same for the shadow
});

// custom destination marker icon
const destinationIcon = L.icon({
  iconUrl: "/images/destinationMarker.png",
  iconSize: [64, 64], // Size of the icon [width, height]
  iconAnchor: [3, 64], // Point of the icon which will correspond to marker's location [half of width, height]
  shadowUrl: "/images/destinationMarkerShadow.png", // URL to the shadow image
  shadowSize: [64, 64], // size of the shadow
  shadowAnchor: [0, 64], // the same for the shadow
});

//--------------------------------------------------------//
//----------------------Custom Sounds---------------------//
//--------------------------------------------------------//

/**
 * Plays a honk sound.
 */
function honk() {
  var sound = new Howl({
    src: ["sounds/honk.mp3"],
  });
  sound.play();
}

/**
 * Plays a swoosh sound.
 */
function swoosh() {
  var sound = new Howl({
    src: ["sounds/flagSound.mp3"],
  });
  sound.play();
}

/**
 * Plays an explosion sound.
 */
function boom() {
  var sound = new Howl({
    src: ["sounds/explosionSound.mp3"],
  });
  sound.play();
}

/**
 * Plays a racing sound.
 */
function race() {
  var sound = new Howl({
    src: ["sounds/calculateSound.mp3"],
  });
  sound.play();
}

//--------------------------------------------------------//
//-------------------GUI Functionality--------------------//
//--------------------------------------------------------//

/**
 * Creates a marker and fills corresponding forms based on the active form.
 * Present markers will slide to the given location and will not be created again.
 * Updates the calculate Button accordingly.
 * The startMarker will honk.
 *
 * @param {number} latitude - The latitude for the marker.
 * @param {number} longitude - The longitude for the marker.
 */
function createMarkerAndFillForms(latitude, longitude) {
  geojsonLayer.clearLayers();
  if (activeButton === startButton) {
    document.getElementById("startLat").value = latitude;
    document.getElementById("startLng").value = longitude;
    if (!markerStart) {
      markerStart = L.marker([latitude, longitude], { icon: startIcon }).addTo(
        map
      );
      markers.push(markerStart);
    }
    honk();
    markerStart.slideTo([latitude, longitude], { duration: 500 });
    markerStart.bounce();
    /* automatically switches to destinationButton as activeButton */
    activeButton = destinationButton;
    destinationButton.classList.add("active");
    startButton.classList.remove("active");
    L.Marker.stopAllBouncingMarkers();
    if (markerDestination) {
      markerDestination.bounce();
    }
  } else {
    document.getElementById("destinationLat").value = latitude;
    document.getElementById("destinationLng").value = longitude;
    if (!markerDestination) {
      markerDestination = L.marker([latitude, longitude], {
        icon: destinationIcon,
      }).addTo(map);
      markers.push(markerDestination);
    }
    swoosh();
    markerDestination.slideTo([latitude, longitude], { duration: 500 });
    markerDestination.bounce();
  }
  updateClearButtonState();
  updateCalculateButtonState();
}

/**
 * Removes a given marker from the map with a visual explosion effect.
 *
 * @param {L.Marker} marker - The marker to be removed.
 */
function removeByExplosion(marker) {
  let latlng = marker.getLatLng();
  marker.remove();
  let explosion = document.createElement("img");
  explosion.src = "images/bigExplosion.gif";

  // Style explosion
  let yOffset = 30; // positive = up
  let xOffset = 10; // positive = left
  explosion.style.position = "absolute";
  explosion.style.width = "64px";
  explosion.style.height = "64px";
  explosion.style.left = map.latLngToLayerPoint(latlng).x - 32 - xOffset + "px";
  explosion.style.top = map.latLngToLayerPoint(latlng).y - 32 - yOffset + "px";
  explosion.style.zIndex = 4;

  map.getPane("overlayPane").appendChild(explosion);

  setTimeout(function () {
    explosion.remove();
  }, 1600);
}

/**
 * Sends coordinates to the backend and retrieves coordinates of the nearest node to these coordinates.
 * Will also fill the forms with coordinates of neares node and place a marker by calling (@see createMarkerAndFillForms )
 * @param {number} latitude - The latitude of the clicked location on the map.
 * @param {number} longitude - The longitude of the clicked location on the map.
 *
 */
function placeMarkersAndFillFormsAtNearestNode(latitude, longitude) {
  fetch("/api/node", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      latitude: latitude,
      longitude: longitude,
    }),
  })
    .then((response) => response.json())
    .then((data) => {
      latitude = data.latitude;
      longitude = data.longitude;
      createMarkerAndFillForms(latitude, longitude);
    });
}

/**
 * Gets and stores values from form fields and creates & returns a JSON object from these values.
 * @returns {{
 *   start_latitude: string,
 *   start_longitude: string,
 *   end_latitude: string,
 *   end_longitude: string
 * }} The JSON object containing latitude and longitude values.
 */
function getFormData() {
  return {
    start_latitude: document.getElementById("startLat").value,
    start_longitude: document.getElementById("startLng").value,
    end_latitude: document.getElementById("destinationLat").value,
    end_longitude: document.getElementById("destinationLng").value,
  };
}

/**
 * Checks if the data form the backend is valid geoJSON. Will show an error if it is not valid.
 * @param {*} data some data from the backend
 * @returns true if the geoJSON data is valid to be shown on the map
 * @see showError
 */
function isValidGeoJSON(data) {
  if (
    data !== null &&
    "features" in data &&
    Array.isArray(data.features) &&
    data.features.length > 0
  ) {
    return true;
  } else {
    showError();
    return false;
  }
}

/**
 * This will make a div with an error message visible for 5 seconds.
 */
function showError() {
  const errorContainer = document.getElementById("errorContainer");
  // Show error container
  errorContainer.classList.remove("hidden");
  // Wait for the next frame to apply the transition
  requestAnimationFrame(() => {
    // Add class to start the fade-in transition
    errorContainer.style.opacity = "1";
  });
  // Wait for 5 seconds and then fade out
  setTimeout(() => {
    errorContainer.style.opacity = "0";
    // Hide the error container after setting the opacity to 0
    setTimeout(() => {
      errorContainer.classList.add("hidden");
    }, 500); // Adjust based on your transition duration
  }, 5000); // Stay visible for 5 seconds
}

// Needed variables for the event listeners
let activeButton = startButton;
let markerStart;
let markerDestination;

// We store the geoJsonLayer (path) in this variable so we can clear it easily (clear button)
let geojsonLayer = L.geoJSON();

startButton.addEventListener("click", handleStartButtonClick);
/**
 * Clicking the start button (the car) will make it possible
 * to place a start marker upon clicking the map.
 * Will make an existing start marker bounce.
 */
function handleStartButtonClick() {
  activeButton = startButton;
  startButton.classList.add("active");
  destinationButton.classList.remove("active");
  L.Marker.stopAllBouncingMarkers();
  if (markerStart) {
    markerStart.bounce();
  }
}

destinationButton.addEventListener("click", handleDestinationButtonClick);
/**
 * Clicking the destination button (the flag) will make it possible
 * to place a destination marker upon clicking the map.
 * Will make an existing destination marker bounce.
 */
function handleDestinationButtonClick() {
  activeButton = destinationButton;
  destinationButton.classList.add("active");
  startButton.classList.remove("active");
  L.Marker.stopAllBouncingMarkers();
  if (markerDestination) {
    markerDestination.bounce();
  }
}

const contextMenu = document.getElementById("contextMenu");
/**
 * Clicking the map will send the coordinates to the backend to find the nearest node
 * and fill the currently active form. If no form is active, nothing will happen.
 * If there is a context menu and the map is clicked, the menu will disappear and nothing else will happen.
 */
map.on("click", function (e) {
  if (contextMenu.style.display === "block") {
    hideContextMenu();
    return;
  }
  if (
    !startButton.classList.contains("active") &&
    !destinationButton.classList.contains("active")
  ) {
    return;
  }
  clickedLatitude = String(e.latlng.lat);
  clickedLongitude = String(e.latlng.lng);
  placeMarkersAndFillFormsAtNearestNode(clickedLatitude, clickedLongitude);
});

/**
 * Hide an existing context menu
 */
function hideContextMenu() {
  contextMenu.style.display = "none";
}

/**
 * These event handlers hide the context menu on dragging and zooming the map.
 */
map.on("zoomstart", hideContextMenu);
map.on("dragstart", hideContextMenu);

document
  .getElementById("calculateButton")
  .addEventListener("click", handleCalculateButtonClick);
/**
 * Clicking the calculate button initiates the calculation of a route based on the form data,
 * displays the calculated route on the map, and fits the map bounds to the route.
 * The calculate button is only clickable if both markers (start & destination) are placed.
 * @see updateCalculateButtonState
 * @see race
 * @see showError
 * @see isValidGeoJSON
 */
function handleCalculateButtonClick() {
  // SETUP START
  clearButton.disabled = true;
  // (loading animation, disabling start & destination button, removes existing routes, stops bouncing of markers and stores form data)
  addAnimationClass(); // Add the animation class before disabling the button for a better look
  startAnimation(); // Takes a while, so it is started asap
  calculateButton.disabled = true;

  startButton.classList.contains("active")
    ? startButton.classList.remove("active")
    : destinationButton.classList.remove("active");
  geojsonLayer.clearLayers();
  L.Marker.stopAllBouncingMarkers();
  const jsonData = JSON.stringify(getFormData());
  // SETUP DONE

  fetch("/api/route", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: jsonData,
  })
    .then((response) => response.json())
    .then((data) => {
      if (isValidGeoJSON(data)) {
        // Style for the GeoJSON path
        const pathStyle = {
          color: "rgb(52, 152, 219)", // Set the line color
          weight: 5, // Set the line thickness
          opacity: 1, // Set the line opacity
        };
        // Create a GeoJSON layer with the custom style
        const path = L.geoJSON(data, {
          style: pathStyle,
        });
        race();
        stopAnimation();
        geojsonLayer = path.addTo(map);
        clearButton.disabled = false;
        // zoom the map to fit the path, add some padding so the path is not behind the gui and the markers dont clip off screen
        // will place the route on the right side of the screen,
        map.fitBounds(path.getBounds(), {
          paddingTopLeft: [500, 50],
          paddingBottomRight: [50, 50],
          duration: 1500,
        });
      } else {
        stopAnimation();
        clearButton.disabled = false;
        // Handle the case when the GeoJSON data is invalid - popup will appear (no route found)
        showError();
      }
    });
}

/**
 * Clicking the clear button clears all form fields on the page,
 * removes markers from the map, disables the calculate button, clears the GeoJSON layer representing
 * the route on the map, and resets the active form to be the start form. Makes the GUI shake.
 * @see boom
 * @see clearAllFormInputs
 * @see updateCalculateButtonState
 * @see shakeElement
 */
document
  .getElementById("clearButton")
  .addEventListener("click", handleClearButtonClick);

function handleClearButtonClick() {
  boom();
  shakeElement("GUI");
  // Trigger the spinning animation
  clearButton.classList.add("spin");
  // Remove the spinning class after the animation completes
  setTimeout(() => {
    clearButton.classList.remove("spin");
  }, 1000); // Duration of the spinning animation

  clearAllFormInputs();

  if (markerStart) {
    removeByExplosion(markerStart);
  }
  if (markerDestination) {
    removeByExplosion(markerDestination);
  }
  markerStart = null;
  markerDestination = null;

  markers = [];
  updateCalculateButtonState();
  //This makes the normal (red/white) background stay for the duration if the spinning animation, updating the button state 1s later (duration of the animation)
  // While it is spinning, pointer events are manually disabled in css (pointer events -> none). This way, the button is not
  // clickable during the animation without being disabled so it can still show its colors! Pretty smart, right?
  setTimeout(function () {
    updateClearButtonState();
  }, 1000);

  geojsonLayer.clearLayers();

  activeButton = startButton;
  startButton.classList.add("active");
  destinationButton.classList.remove("active");
}

/**
 * Makes the html element with the given ID shake for 0,5s.
 * @param elementId as String
 */
function shakeElement(elementId) {
  const element = document.getElementById(elementId);
  if (element) {
    element.classList.add("shake");
    // Reset the shaking class after the animation completes
    setTimeout(() => {
      element.classList.remove("shake");
    }, 500);
  } else {
    console.error(`Element with ID '${elementId}' not found.`);
  }
}

//--------------------------------------------------------//
//------------------Header Hover Effect-------------------//
//--------------------------------------------------------//

// The following code makes for a cool effect on the header by randomly replacing the letters and then reverting back to the original header.
const letters = "abcdefghijklmnopqrstuvwxyz";
let interval = null;
document.querySelector("h1").onmouseover = (event) => {
  let iteration = 0;
  clearInterval(interval);
  interval = setInterval(() => {
    event.target.innerText = event.target.innerText
      .split("")
      .map((letter, index) => {
        if (index < iteration) {
          return event.target.dataset.value[index];
        }
        return letters[Math.floor(Math.random() * letters.length)];
      })
      .join("");
    if (iteration >= event.target.dataset.value.length) {
      clearInterval(interval);
    }
    iteration += 1;
  }, 50); // animation speed
};

//--------------------------------------------------------//
//-------------------Loading Animation--------------------//
//--------------------------------------------------------//

const car = document.createElement("div");
car.className = "car";
calculateButton.appendChild(car);

// Add a class to the button when the animation is in progress
function addAnimationClass() {
  calculateButton.classList.add("animation-in-progress");
}

// Remove the class when the animation is complete
function removeAnimationClass() {
  calculateButton.classList.remove("animation-in-progress");
}

function moveCar() {
  const calculateButtonWidth = calculateButton.offsetWidth;
  const carWidth = car.offsetWidth;

  car.style.transition = "none";
  car.style.transform = `translate(-${carWidth}px, -50%)`; // Move the car outside the button to the left
  calculateButton.offsetWidth;
  car.style.transition = "transform 1s linear";

  addAnimationClass(); // Add the class when the animation starts

  requestAnimationFrame(() => {
    car.style.transform = `translate(${calculateButtonWidth}px, -50%)`; // Move the car outside the button to the right
  });
}

function resetCar() {
  car.style.transition = "none";
  car.style.transform = "translate(-120%, -50%)"; // Adjust the initial position to be outside the button
  calculateButton.offsetWidth;
  car.style.transition = "transform 0s linear"; // Reset transition duration to 0s
}

let animationInterval; // Variable to store the interval ID

/**
 * This function starts the loading animation on the calculate button. Will be called when clicking the calculate button.
 */
function startAnimation() {
  animationInterval = setInterval(moveCar, 1000); // 1 second interval, adjust as needed
}

/**
 * This function stops the loading animation. Is called when backend sends response to pressing the calculate button.
 */
function stopAnimation() {
  clearInterval(animationInterval);
  resetCar(); // Reset the car position when stopping the animation
  removeAnimationClass(); // Remove the animation class
}

//--------------------------------------------------------//
//---------------------Context Menu-----------------------//
//--------------------------------------------------------//

// Get references to HTML elements
const containerToRightClick = document.getElementById("map");
// setup a variable where the rightClicked Coordinates are stored, so it is available outside the event listener for the right click
let rightClickedLatLng; // Variable to store the right-clicked coordinates
const menuItem1 = document.getElementById("menuItem1");
const menuItem2 = document.getElementById("menuItem2");
const menuItem3 = document.getElementById("menuItem3");

// Event listener for right-click on the Leaflet map
map.on("contextmenu", (e) => {
  e.originalEvent.preventDefault();
  contextMenu.style.display = "none";
  contextMenu.offsetHeight; //hacky forced reflow to make the animation visible when doing *right click* then *right click* somewhere else.

  const { clientX, clientY } = e.originalEvent;
  showContextMenu(clientX, clientY);
  // Store the right-clicked coordinates
  rightClickedLatitude = String(e.latlng.lat);
  rightClickedLongitude = String(e.latlng.lng);
});

// Function to display the context menu at the specified position
function showContextMenu(x, y) {
  // Display the context menu.
  contextMenu.style.display = "block";

  // Get the width and height of the viewport
  const { clientWidth, clientHeight } = document.documentElement;

  // Determine if the click is in the top half or bottom half of the screen
  const isTop = y <= clientHeight / 2;
  // Determine if the click is in the left half or right half of the screen
  const isLeft = x <= clientWidth / 2;

  // Adjust the position of the context menu based on the anchor position
  contextMenu.style.top = isTop
    ? `${y}px`
    : `${y - contextMenu.clientHeight}px`;
  contextMenu.style.left = isLeft
    ? `${x}px`
    : `${x - contextMenu.clientWidth}px`;

  // Set border-radius based on the anchor position
  setBorderRadius(isTop, isLeft);

  // Set transform-origin based on the anchor position
  setTransformOrigin(isLeft, isTop);
}

// Function to set border-radius based on anchor position
function setBorderRadius(isTop, isLeft) {
  // Reset default border-radius
  contextMenu.style.borderRadius = "5px 5px 5px 5px";

  // Determine the anchor position and set border-radius accordingly
  if (isTop && isLeft) {
    contextMenu.style.borderRadius = "0 10px 10px 10px";
  } else if (isTop && !isLeft) {
    contextMenu.style.borderRadius = "10px 0 10px 10px";
  } else if (!isTop && !isLeft) {
    contextMenu.style.borderRadius = "10px 10px 0 10px";
  } else if (!isTop && isLeft) {
    contextMenu.style.borderRadius = "10px 10px 10px 0";
  }
}

// Function to set transform-origin based on anchor position
function setTransformOrigin(isLeft, isTop) {
  // Determine the transform origin based on the anchor position
  let transformOriginX = isLeft ? "left" : "right";
  let transformOriginY = isTop ? "top" : "bottom";

  // Set the transform origin dynamically
  contextMenu.style.transformOrigin = `${transformOriginX} ${transformOriginY}`;
}

// Event listeners for menu item clicks
menuItem1.addEventListener("click", menuItem1ClickHandler);

function menuItem1ClickHandler() {
  activeButton = startButton;
  startButton.classList.add("active");
  destinationButton.classList.remove("active");
  L.Marker.stopAllBouncingMarkers();
  if (markerStart) {
    markerStart.bounce();
  }
  placeMarkersAndFillFormsAtNearestNode(
    rightClickedLatitude,
    rightClickedLongitude
  );
  contextMenu.style.display = "none";
}

menuItem2.addEventListener("click", menuItem2ClickHandler);

function menuItem2ClickHandler() {
  activeButton = destinationButton;
  destinationButton.classList.add("active");
  startButton.classList.remove("active");
  L.Marker.stopAllBouncingMarkers();
  if (markerDestination) {
    markerDestination.bounce();
  }
  placeMarkersAndFillFormsAtNearestNode(
    rightClickedLatitude,
    rightClickedLongitude
  );
  contextMenu.style.display = "none";
}

menuItem3.addEventListener("click", menuItem3ClickHandler);

function menuItem3ClickHandler() {
  console.log("Surprise!");

  window.open(
    //https://www.youtube.com/watch?v=dQw4w9WgXcQ (this is the original rick roll, but may play an ad before)
    "https://www.youtube.com/watch?v=xvFZjo5PgG0&ab_channel=Duran&autoplay=1",
    "_blank"
  );
  contextMenu.style.display = "none";
}

menuItem4.addEventListener("click", menuItem4ClickHandler);

function menuItem4ClickHandler() {
  contextMenu.style.display = "none";
  handleCalculateButtonClick();
}
