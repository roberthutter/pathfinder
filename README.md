# Pathfinder - A Simple Dijkstra Based Route Calculator

The Route Planner Project aims to develop a user-friendly application that assists travelers in determining the most efficient route between two or more destinations. It consists of a backend HTTP web server and a basic frontend interface for querying and displaying routes.

It was developed by Luca Schütt, Ramon Floor and Robert Hutter, students of the University of Stuttgart (Germany) as part of the module "Programmierprojekt".

*Copyright (C) 2023 Luca Schütt, Ramon Floor, Robert Hutter*

Free to use for educational and academic purposes. For commercial usage please contact us at robert.hutter@dobsinalia.com.

The software is provided "as is" and without liability of any kind.

> **Note:** This project includes third party sources. A different license may apply to these parts of the software. See
>
> * *src/spq* from Prof. Dr. Peter Sanders https://algo2.iti.kit.edu/
> * Mongoose HTTP Server https://mongoose.ws/

# How to Build
To build this project you will need a C++ development environment including a C++ compiler and linker program.

**Supported environments:** Linux, MacOS.

**Tested on:** Linux Ubuntu 22.04.3 LTS and MacOS Sonoma 14.2.

### Build Tutorial (for Linux Ubuntu)
1. Clone this repository or download the latest release of the source code.
2. Install gcc and make using `sudo apt install gcc make`.
3. Verify your gcc installation by trying `gcc`, `g++` and `cpp`. Do not continue until these commands or alternatives are available.
4. Open a terminal window in the cloned folder. In this folder you should find the makefile.
5. Run `make` to build.

## System Requirements
* A Unix based operating system. Tested on Linux Ubuntu and MacOS.
* At least as much RAM as the size of the map file being used.
* For the `germany.fmi` map from https://fmi.uni-stuttgart.de/alg/research/stuff/ at least 2.5 GB RAM.

# Usage Tutorial
1. Download a compatible map file. A such map of Germany is available at https://fmi.uni-stuttgart.de/alg/research/stuff/. For the required file format see the section *Map File Format*.
2. Start the backend program using a terminal. For more information on commands see the section *Command Line Interface*.

# Command Line Interface
The backend program can be configured and tested using a command line interface. This is specified as follows:

## Required Switches

- `-graph <graph_file_path>`
  - **Description**: Specifies the location of the graph representing the map.
  - **Syntax**: `-graph /path/to/graph/file`
  - **Note**: This switch must always come before `-socket`, `-que`, `-lon`, `-lat`, `-s` switches.

## Optional Switches

1. **Socket Mode**
   - `-socket <port_number>`
     - **Description**: Allows the backend to listen to incoming requests from the frontend on the specified port. This switch is exclusive and no other switches should follow it.
     - **Syntax**: `-socket 8080`

2. **Route Query Mode**
   - `-que <path_to_que_file>`
     - **Description**: Specifies the location of the file that contains source and destination node pairs for route calculations.
     - **Syntax**: `-que /path/to/que/file`
     - **Note**: This switch can be combined with others mentioned below except for `-socket`.

3. **Benchmark Mode**
   - `-benchmark`
     - **Description**: Activates the benchmark mode which assesses the performance of all preceding operations.
     - **Syntax**: `-benchmark`
     - **Note**: To benchmark the map loader, use this switch before `-graph`.

4. **Nearest Node Identification Mode**
   - `-lon <longitude_value> -lat <latitude_value>`
     - **Description**: Provides longitude and latitude coordinates. The system will identify and return the closest node to the provided coordinates.
     - **Syntax**: `-lon 9.098 -lat 48.746`

5. **One-To-All Test Mode**
   - `-s <source_node_id>`
     - **Description**: Initiates the one-to-all Dijkstra algorithm starting from the specified source node. Once the process concludes, the system will prompt the user to input a destination node. It will then return the distance between the source and destination nodes based on the previously executed one-to-all operation.
     - **Syntax**: `-s 638394`

## Usage Notes

1. The command line arguments are evaluated in the order they are present in.
2. The `-graph` switch is mandatory as the first switch for every operation with the exception of `-benchmark`. All other switches are optional and are utilized based on the operation mode desired by the user.
3. The `-socket` switch should not be combined with any other switches. If provided, it should be the last switch in the command.

## Usage Examples

1. **Activating Socket Mode**: Start the backend in socket mode to listen on port 8080:
   
`program_name -graph /home/user/maps/europe.fmi -socket 8080`

2. **Processing Route Queries from a File**: Calculate routes for pairs of source and destination nodes specified in a .que file:

`program_name -graph /home/user/maps/europe.fmi -que /home/user/queries/route_queries.que`

3. **Identifying the Closest Node to Given Coordinates**: Find the nearest node to a specific set of longitude and latitude coordinates:

`program_name -graph /home/user/maps/europe.fmi -lon 9.098 -lat 48.746`

4. **Running One-to-All Dijkstra from a Specific Node and Activating Socket Mode**: Start the one-to-all Dijkstra algorithm from node 638394, and then starts the backend in socket mode to listen on port 8080.

`program_name -graph /home/user/maps/europe.fmi -s 638394 -socket 8080`

5. **Combining Multiple Operations**: Load a graph, find the nearest node to given coordinates, and then process route queries from a .que file:

`program_name -graph /home/user/maps/europe.fmi -lon 9.098 -lat 48.746 -que /home/user/queries/route_queries.que`


6. Load a graph, and benchmark the one-to-all Dijkstra algorithm from a specified node:

`program_name -benchmark -graph /home/user/germany.fmi -lon 9.098 -lat 48.746 -que /home/user/germany.que -s 638394`


> **Note**: `program_name` is a placeholder and should be replaced with the actual name of the program or executable. Similarly, paths such as `/home/user/maps/europe.fmi`

# Troubleshooting
Before contacting us for support due to an issue, please read check out this section containing typical issues and fixes.

1. **Loading the map file leads to the error message** `Reading graph file and creating graph data structure (...)
Error loading the graph from the specified path.`

    Double check the provided file path and ensure that the file format is conform with the specification below.

2. **Segmentation Fault**

    Double check the file format of the provided map file to make sure it is conform with the specification below.
    
    Make sure the program has enough memory available to run (see *System Requirements*), **as a segmentation fault can be caused by running out of memory during the phase in which the data structure needed for nearest node calculation is constructed.**

3. **Failed to start HTTP server, socket unavailable.**

    Double check the given port number is available (port numbers below 1024 are typically reserved for sudo users). Try the ports 8080, 8081, 8082.

If the issue is not one of the above, or if the proposed solution does not work, open an issue, or contact us at robert.hutter@dobsinalia.com.

# Map File Format
The program supports map files of the following format:
* exactly 4 lines starting with `#` which contain header information.
* one empty line
* line containing the total number of nodes
* line containing the total number of edges
* for each node: nodeID nodeID2 latitude longitude elevation
* for each edge: srcIDX trgIDX cost type maxspeed

## Example Map File
```
# Id : 0
# Timestamp : 1495108694
# Type : maxspeed
# Revision : 1

5
9
0 100 49.00 10.00 0
1 101 49.01 10.01 0
2 102 49.02 10.02 0
3 103 49.03 10.03 0
4 104 49.04 10.04 0
0 1 10 3 50
0 2 5 3 50
1 2 2 3 50
1 3 1 3 50
2 1 3 3 50
2 4 2 3 50
3 4 5 3 50
4 0 7 3 50
4 3 6 3 50
```
> **Note:** Due to development time constraints no file format validation was implemented. Using a map file with a different format then the one specified above will lead to undefined behavior, likely leading to a segmentation fault.