var searchData=
[
  ['name_0',['name',['../structmg__http__header.html#aed5621a6d77e25fc8ae984593a22c566',1,'mg_http_header::name'],['../structmg__http__part.html#a39efe9a2d61cc90b639ea702d444dfa8',1,'mg_http_part::name'],['../structmg__tls__opts.html#a89148781eee065b55d14e6c267173428',1,'mg_tls_opts::name'],['../structmg__dns__message.html#acac476dc39a798e599c1a1e65462a5f4',1,'mg_dns_message::name']]],
  ['next_1',['next',['../structdns__data.html#a38ab656e29a9dac3c4f722ef4385a683',1,'dns_data::next'],['../structmg__timer.html#aa5ad53dd060c5409242dd46ef0d37e4f',1,'mg_timer::next'],['../structmg__connection.html#afcfd89f119a87cba6f6dfec2d2eda5d9',1,'mg_connection::next'],['../structmg__rpc.html#a61d7c92a83ea4fe9bd61b087878f7768',1,'mg_rpc::next']]],
  ['nextid_2',['nextid',['../structmg__mgr.html#a5767d57547d9ca7c653596a56f0b237a',1,'mg_mgr']]],
  ['nlen_3',['nlen',['../structmg__dns__rr.html#aab57e63a51776c2b631d3ad8693ba70c',1,'mg_dns_rr']]],
  ['nodeid_4',['nodeID',['../struct_q_t_node.html#a7c2489df3d47df8a4884976deca303cb',1,'QTNode']]],
  ['nodes_5',['nodes',['../struct_graph.html#a19c47a8b645646a87c21c5a0ed86f29a',1,'Graph']]],
  ['num_5fanswers_6',['num_answers',['../structmg__dns__header.html#a2d577357775702ca340492ca51379b21',1,'mg_dns_header']]],
  ['num_5fauthority_5fprs_7',['num_authority_prs',['../structmg__dns__header.html#a90a7621286acf1c8b78d3ee450dce9b6',1,'mg_dns_header']]],
  ['num_5fother_5fprs_8',['num_other_prs',['../structmg__dns__header.html#aed8714aa60f2cc79dc0c81378c2ddb50',1,'mg_dns_header']]],
  ['num_5fprops_9',['num_props',['../structmg__mqtt__opts.html#a67400de725dba626ff01fda95d62715b',1,'mg_mqtt_opts']]],
  ['num_5fquestions_10',['num_questions',['../structmg__dns__header.html#a156d3f5926d1fdb24bcbcba1f273c59a',1,'mg_dns_header']]],
  ['num_5fwill_5fprops_11',['num_will_props',['../structmg__mqtt__opts.html#ac7b4f09aef5f0e0b2a48abd565083b76',1,'mg_mqtt_opts']]],
  ['numberofedges_12',['numberOfEdges',['../struct_graph.html#a36bc1d011537db88ec5c6c8a820dbeed',1,'Graph']]],
  ['numberofnodes_13',['numberOfNodes',['../struct_graph.html#ae95a6e44dd7f0ec0dfec35889d16ac4f',1,'Graph']]]
];
