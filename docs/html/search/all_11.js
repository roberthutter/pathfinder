var searchData=
[
  ['s2ptr_0',['S2PTR',['../mongoose_8c.html#aed0270b04514e7f7a93d1eb9931017d3',1,'mongoose.c']]],
  ['sa_1',['sa',['../unionusa.html#a3bc7f8512f2fb0e7ef3e8c69dd794df1',1,'usa']]],
  ['scope_5fid_2',['scope_id',['../structmg__addr.html#ad12d0c0d17535b7faafd68b1c8270d74',1,'mg_addr']]],
  ['send_3',['send',['../structmg__connection.html#a8e48bb473bd7554101dacf968b487721',1,'mg_connection']]],
  ['serverstatus_4',['serverstatus',['../pathfinder__server_8h.html#a23331e5df325fea12f9a553d3b1cab4a',1,'ServerStatus:&#160;pathfinder_server.h'],['../pathfinder__server_8h.html#a97e74c6b6dbb5735e3a90d12c038ea01',1,'ServerStatus:&#160;pathfinder_server.h']]],
  ['setupclosestnodedatastructure_5',['setupclosestnodedatastructure',['../pathfinder_8cpp.html#a5c42542e2c15f02fc777c361ea797dc6',1,'setupClosestNodeDataStructure(void):&#160;pathfinder.cpp'],['../pathfinder_8h.html#a5c42542e2c15f02fc777c361ea797dc6',1,'setupClosestNodeDataStructure(void):&#160;pathfinder.cpp']]],
  ['sin_6',['sin',['../unionusa.html#a680bc429e90923ecbbf9c7d38ba4bf26',1,'usa']]],
  ['size_7',['size',['../structmg__queue.html#a4f7593f21c0175369e685d218d849976',1,'mg_queue::size'],['../structmg__iobuf.html#a67b0496b2513bb75248311ac88c678c2',1,'mg_iobuf::size'],['../structpacked__file.html#a9ec90d618b6dacc433f74550c52337ad',1,'packed_file::size']]],
  ['sk_8',['sk',['../structmg__fs.html#ae3b3b01e47b5b3675f2aa1ec40629fb2',1,'mg_fs']]],
  ['sntp_5fmax_5ffrac_9',['SNTP_MAX_FRAC',['../mongoose_8c.html#a00bd3db195744826a9444802cb754bba',1,'mongoose.c']]],
  ['sntp_5ftime_5foffset_10',['SNTP_TIME_OFFSET',['../mongoose_8c.html#a5f430c62e56033fb6a909f5448e2a20e',1,'mongoose.c']]],
  ['socket_5funavailable_11',['SOCKET_UNAVAILABLE',['../pathfinder__server_8h.html#a23331e5df325fea12f9a553d3b1cab4aaea950c25a48f0e345270627e24a53287',1,'pathfinder_server.h']]],
  ['sol_5ftcp_12',['SOL_TCP',['../mongoose_8c.html#ac65409d904781e5a23529bd6bef2b673',1,'mongoose.c']]],
  ['ssi_5fpattern_13',['ssi_pattern',['../structmg__http__serve__opts.html#a4d297e8fbef671c5162ff85c4bad8e47',1,'mg_http_serve_opts']]],
  ['st_14',['st',['../structmg__fs.html#acef078be216a94b0b64ce4858a5d617d',1,'mg_fs']]],
  ['startpathfinderserver_15',['startpathfinderserver',['../pathfinder__server_8h.html#a7730a006a39be21c2b81bb2443105190',1,'startPathfinderServer(const PortNumber socket):&#160;pathfinder_server.cpp'],['../pathfinder__server_8cpp.html#a7730a006a39be21c2b81bb2443105190',1,'startPathfinderServer(const PortNumber socket):&#160;pathfinder_server.cpp']]],
  ['state_16',['state',['../structmg__sha1__ctx.html#a28f48addb9f4840006dfd7c3793a6c4d',1,'mg_sha1_ctx']]],
  ['successfully_5fterminated_17',['SUCCESSFULLY_TERMINATED',['../pathfinder__server_8h.html#a23331e5df325fea12f9a553d3b1cab4aa6a7ccd42c27090c782a0e97926b808a9',1,'pathfinder_server.h']]]
];
