var indexSectionsWithContent =
{
  0: "cdegilmnpqstu",
  1: "cegmq",
  2: "dp",
  3: "cdgilsu",
  4: "cdelmnpt",
  5: "s",
  6: "su",
  7: "cnp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros"
};

