#!/bin/bash

rm -f benchmark.log
touch benchmark.log
for i in {1..25}; do
    echo "Run $i" | tee -a benchmark.log
    ./build/pathfinder -benchmark -graph maps/germany.fmi -no-input -s 638394 >> benchmark.log 2>&1
done
python3 eval_benchmark_data.py