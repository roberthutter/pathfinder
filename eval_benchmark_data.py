import re
import statistics

def main():
    # Call the function with the path to your log file
    log_file_path = 'benchmark.log'
    graph_read_times, dijkstra_times = extract_times(log_file_path)

    # Calculate medians
    median_graph_read_time = calculate_median(graph_read_times)
    median_dijkstra_time = calculate_median(dijkstra_times)

    # Print the results
    print("Graph Read Times (ms):", graph_read_times)
    print("Median Graph Read Time (ms):", median_graph_read_time)
    print("One-to-all Dijkstra Times (ms):", dijkstra_times)
    print("Median One-to-all Dijkstra Time (ms):", median_dijkstra_time)

def extract_times(log_file_path):
    # Define the regex patterns to find the times
    graph_read_pattern = r"graph read took (\d+)ms"
    dijkstra_pattern = r"one-to-all Dijkstra took (\d+)ms"

    # Read the log file
    with open(log_file_path, 'r') as file:
        log_content = file.read()

    # Extract times using the regex patterns
    graph_read_times = [int(match) for match in re.findall(graph_read_pattern, log_content)]
    dijkstra_times = [int(match) for match in re.findall(dijkstra_pattern, log_content)]

    return graph_read_times, dijkstra_times

def calculate_median(values_list):
    # Calculate and return the median value
    return statistics.median(values_list)

if __name__ == "__main__":
    main()