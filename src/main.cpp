/**
 * @file main.cpp
 * @brief Main program including the Command Line Interface of the Pathfinder library.
 * 
 * @author Robert Hutter
 * @date 10/27/2023
*/
#include <getopt.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#ifndef __APPLE__
#include <bits/time.h>
#endif
#include "pathfinder/pathfinder.h"
#include "pathfinder/pathfinder_server.h"

static const int GRAPH_OPTION_ID = 1;
static const int SOCKET_OPTION_ID = 2;
static const int QUE_OPTION_ID = 3;
static const int LON_OPTION_ID = 4;
static const int LAT_OPTION_ID = 5;
static const int SOURCE_OPTION_ID = 6;
static const int HELP_OPTION_ID = 7;

static const int NO_MORE_OPTION_PRESENT = -1;
static const int OPTION_NOT_RECOGNIZED = 0;
static const int FLAG_OPTION_RECOGNIZED = 0;
static const int UNRECOGNIZED_OPTION = '?';

static int benchmarkModeEnabled = false;
static int noInputModeEnabled = false;

static const struct option commandOptions[] = {
    {"graph", required_argument, NULL, GRAPH_OPTION_ID},
    {"socket", required_argument, NULL, SOCKET_OPTION_ID},
    {"que", required_argument, NULL, QUE_OPTION_ID},
    {"benchmark", no_argument, &benchmarkModeEnabled, true},
    {"lon", required_argument, NULL, LON_OPTION_ID},
    {"lat", required_argument, NULL, LAT_OPTION_ID},
    {"s", required_argument, NULL, SOURCE_OPTION_ID},
    {"help", no_argument, NULL, HELP_OPTION_ID},
    {"no-input", no_argument, &noInputModeEnabled, true},
    {NULL, 0, NULL, 0}
};

static bool tryToLoadGraph (const char *const graphFile);
static bool tryToStartServer (const char *const socket);
static void tryToDoRouteQueries (const char *const queFileName);
static void tryNearestNodeIdentification (const Coordinate coordinate);
static void tryOneToAllDijkstraOperation (const NodeID startNodeId);
static void printOptionUsage (const int optionId);
static void printHelp (void);
static bool processOtherOptions (int argc, char **argv, int *index);

int main (int argc, char **argv) {
    printf ("Pathfinder Backend %s by Luca Schuett, Ramon Floor and Robert Hutter.\n\n", PATHFINDER_VERSION);
    int index = 0;
    int optionId = getopt_long_only (argc, argv, "", commandOptions, &index);
    int exitStatus = EXIT_FAILURE;

    if (optionId != HELP_OPTION_ID) {
        while (optionId == FLAG_OPTION_RECOGNIZED) {
            optionId = getopt_long_only (argc, argv, "", commandOptions, &index);
        }

        if (optionId == GRAPH_OPTION_ID) {
            if (tryToLoadGraph (optarg)) {
                if (processOtherOptions (argc, argv, &index)) {
                    exitStatus = EXIT_SUCCESS;
                }
                unloadGraph ();
            }
        } else if (optionId == NO_MORE_OPTION_PRESENT) {
            printf("Error: no graph file specified using -graph. For more information see -help.\n");
        } else if (optionId == UNRECOGNIZED_OPTION) {
            if (optopt == GRAPH_OPTION_ID) {
                printOptionUsage (optopt);
            } else {
                printf ("For more information see -help.\n");
            }
        } else if (optionId != GRAPH_OPTION_ID) {
            printf ("Error: -graph must be the first option. For more information see -help.\n");
        } else {
            ;
        }
    } else {
        printHelp();
        exitStatus = EXIT_SUCCESS;
    }
    return exitStatus;
}

/**
 * \brief Processes command-line options passed to the program.
 *
 * The function iterates through all the options provided until there 
 * are no more options to process or an unrecognized option is encountered. 
 * In case of an unrecognized option, appropriate error messages will be displayed.
 *
 * \param[in] argc The number of arguments passed to the program.
 * \param[in] argv An array containing the command-line arguments.
 * \param[out] index A pointer to an integer variable that will store 
 *                   the index of the next unprocessed option.
 * \warning Ensure that the commandOptions array is correctly defined and initialized.
 * \return True if all options are successfully processed, false if one reports an error.
 */
static bool processOtherOptions (int argc, char **argv, int *index) {
    Coordinate coordinate;
    bool oneCoordAlreadyReady = false;
    long nodeId;
    char *endChar;
    bool successful = true;

    int optionId = getopt_long_only (argc, argv, "", commandOptions, index);
    while (optionId != NO_MORE_OPTION_PRESENT && successful) {
        switch (optionId) {
            case SOCKET_OPTION_ID:
                successful = tryToStartServer (optarg);
                break;

            case QUE_OPTION_ID:
                tryToDoRouteQueries (optarg);
                break;

            case LAT_OPTION_ID:
                coordinate.latitude = strtod (optarg, &endChar);
                if (endChar != optarg && *endChar == '\0') {
                    if (oneCoordAlreadyReady) {
                        tryNearestNodeIdentification (coordinate);
                    }
                    oneCoordAlreadyReady = !oneCoordAlreadyReady;
                } else {
                    printOptionUsage (optionId);
                    successful = false;
                }
                break;

            case LON_OPTION_ID:
                coordinate.longitude = strtod (optarg, &endChar);
                if (endChar != optarg && *endChar == '\0') {
                    if (oneCoordAlreadyReady) {
                        tryNearestNodeIdentification (coordinate);
                    }
                    oneCoordAlreadyReady = !oneCoordAlreadyReady;
                } else {
                    printOptionUsage (optionId);
                    successful = false;
                }
                break;

            case SOURCE_OPTION_ID:
                nodeId = strtol (optarg, &endChar, 10);
                if (endChar != optarg && *endChar == '\0') {
                    if (0 <= nodeId && nodeId <= getLargestNodeID ()) {
                        tryOneToAllDijkstraOperation ((NodeID) nodeId);
                    } else {
                        printf ("Skipping source due to node ID out of bounds.\n");
                    }
                } else {
                    printOptionUsage (optionId);
                    successful = false;
                }
                break;

            case UNRECOGNIZED_OPTION:
                if (optopt == OPTION_NOT_RECOGNIZED) {
                    printf ("Use -help for more information.\n");
                } else {
                    printOptionUsage(optopt);
                }
                successful = false;
                break;

            case GRAPH_OPTION_ID:
            case FLAG_OPTION_RECOGNIZED:
            case HELP_OPTION_ID:
            default:
                break;
        }
        if (successful) {
            optionId = getopt_long_only (argc, argv, "", commandOptions, index);
        }
    }
    return successful;
}

/**
 * \brief Attempts to load the graph from the specified file.
 *
 * This function tries to load the map graph from the given file path. 
 * If benchmarking is enabled, the function will measure the time taken 
 * to load the graph and display the benchmarking information on the console. 
 * In the event of a failure during the loading process, appropriate error 
 * information will be printed to the console.
 *
 * \param[in] graphFile The file path to the graph representation.
 * \return True if successful, otherwise false.
 */
static bool tryToLoadGraph (const char *const graphFile) {
    printf ("Reading graph file and creating graph data structure (%s)\n", graphFile);
    struct timespec start, end;
    clock_gettime (CLOCK_MONOTONIC_RAW, &start);
    bool successful = false;

    if (loadGraph (graphFile)) {
        if (benchmarkModeEnabled) {
            clock_gettime (CLOCK_MONOTONIC_RAW, &end);
            const unsigned long milliseconds = (end.tv_sec - start.tv_sec) * 1000 + (end.tv_nsec - start.tv_nsec) / 1000000;
            printf ("\tgraph read took %lums\n", milliseconds);
        }

        printf ("Setting up closest node data structure...\n");

        if (setupClosestNodeDataStructure ()) {
            successful = true;
        } else {
            printf ("Error setting up the closest node data structure.\n");
        }
    } else {
        printf ("Error loading the graph from the specified path.\n");
    }
    return successful;
}

/**
 * \brief Attempts to start the Pathfinder HTTP server on the specified port.
 *
 * This function tries to parse a valid port number from the provided string. 
 * Upon successfully parsing a valid port, it will then attempt to start the 
 * Pathfinder HTTP server on that port. If any error occurs during parsing or 
 * starting the server, appropriate error information will be printed to the 
 * console, and the program will be terminated.
 *
 * \param socket A string containing the desired port number for the server.
 * \note Ensure a valid graph is loaded before invoking this function.
 * \warning The provided socket string must represent a valid port number 
 *          typically in the range 1024 to 65535. However, depending on the system 
 *          and privileges, other port numbers might be valid as well.
 */
static bool tryToStartServer(const char *const socket) {
    char *endChar;
    const long port = strtol (socket, &endChar, 10);
    bool successful = false;
    
    if (endChar != socket && *endChar == '\0' &&
        0 < port && port < 65536
    ) {
        switch (startPathfinderServer (port)) {
            case SUCCESSFULLY_TERMINATED:
                break;
            
            case SOCKET_UNAVAILABLE:
                printf ("Failed to start HTTP server, socket unavailable.\n");
                break;
            
            default:
                printf ("An unknown error has occurred trying to start the HTTP server.\n");
                break;
        }
        successful = true;
    } else {
        printOptionUsage (SOCKET_OPTION_ID);
    }
    return successful;
}

/**
 * \brief Processes route queries from a specified file.
 *
 * This function takes the path to a file as input, where the file should contain 
 * pairs of source and destination node IDs. It reads the file and performs route 
 * calculations for each pair of nodes, then outputs the calculated distances to
 * a file with the same name ending in `.sol`.
 *
 * \param queFileName The path to the file containing the source and destination node 
 *                pairs for route calculations.
 * \note The file at the specified path should be in the correct format, with each 
 *       line containing a pair of node IDs separated by whitespace or another 
 *       delimiter.
 * \note This function should only be called if a valid graph has been loaded.
 */
static void tryToDoRouteQueries(const char *const queFileName) {
    printf ("Running one-to-one Dijkstras for queries in .que file %s\n", queFileName);

    FILE *queFile = fopen(queFileName, "r");
    if (queFile != NULL) {
        struct timespec start, end;
        clock_gettime (CLOCK_MONOTONIC_RAW, &start);

        char *line = NULL;
        size_t lineCap = 0;
        while (getline (&line, &lineCap, queFile) > 0) {
            NodeID source, destination;
            if (sscanf (line, "%u %u", &source, &destination) == 2) {
                if (source <= getLargestNodeID () && destination <= getLargestNodeID ()) {
                    if (doPointToPointDijkstra (source, destination)) {
                        printf ("%u\n", getDistance (destination));
                    } else {
                        printf ("Skipping line due to no route found: %u %u\n", source, destination);
                    }
                } else {
                    printf ("Skipping line due to node ID out of bounds.\n");
                }
            } else {
                printf ("Skipping line due to bad format: %s\n", line);
            }
        }

        if (benchmarkModeEnabled) {
            clock_gettime (CLOCK_MONOTONIC_RAW, &end);
            const unsigned long milliseconds = (end.tv_sec - start.tv_sec) * 1000 + (end.tv_nsec - start.tv_nsec) / 1000000;
            printf ("\tprocessing .que file took %lums\n", milliseconds);
        }

        free (line);
        fclose(queFile);
    } else {
        printf ("Failed to open the specified .que file.\n");
    }
}

/**
 * \brief Performs nearest node identification for the given coordinate.
 *
 * This function attempts to identify the node nearest to the specified 
 * coordinate. If the system is in benchmark mode, the time taken to 
 * complete this operation will be measured and reported.
 *
 * \param coordinate The Coordinate structure containing the longitude and latitude 
 *                   values for which the nearest node identification needs to be performed.
 * \note Ensure that a valid graph is loaded into the system before calling 
 *       this function, as the operation is dependent on the internal graph.
 */
static void tryNearestNodeIdentification(const Coordinate coordinate) {    
    printf ("Finding closest node to coordinates %f %f\n", coordinate.longitude, coordinate.latitude);
    struct timespec start, end;
    clock_gettime (CLOCK_MONOTONIC_RAW, &start);

    const Coordinate result = getClosestNodeCoordinate (coordinate);

    if (benchmarkModeEnabled) {
        clock_gettime (CLOCK_MONOTONIC_RAW, &end);
        const unsigned long milliseconds = (end.tv_sec - start.tv_sec) * 1000 + (end.tv_nsec - start.tv_nsec) / 1000000;
        printf ("\tfinding node took %lums: %f, %f\n", milliseconds, result.longitude, result.latitude);
    } else {
        printf ("\tclosest node coordinates: lon %f lat %f\n", result.longitude, result.latitude);
    }
}

/**
 * \brief Performs the Dijkstra path finding operation in one-to-all mode from the specified node.
 *
 * This function initiates the Dijkstra path finding algorithm from the given startNodeId 
 * in a one-to-all mode. After the initial operation completes, the user is prompted 
 * to provide a destination node ID. The function will then determine and print the 
 * distance between the starting node and the user-specified destination node, leveraging 
 * the results from the previously executed Dijkstra algorithm.
 * 
 * If benchmark mode is enabled, the function will also measure and display the time 
 * taken to execute the Dijkstra algorithm.
 *
 * \param startNodeId The ID of the node from which the Dijkstra path finding operation will start.
 * \note Ensure a valid graph is loaded and the specified startNodeId exists within the graph 
 *       before invoking this function.
 */
static void tryOneToAllDijkstraOperation(const NodeID startNodeId) {
    printf ("Computing one-to-all Dijkstra from node id %d\n", startNodeId);
    struct timespec start, end;
    clock_gettime (CLOCK_MONOTONIC_RAW, &start);

    doOneToAllDijkstra (startNodeId);

    if (benchmarkModeEnabled) {
        clock_gettime (CLOCK_MONOTONIC_RAW, &end);
        const unsigned long milliseconds = (end.tv_sec - start.tv_sec) * 1000 + (end.tv_nsec - start.tv_nsec) / 1000000;
        printf ("\tone-to-all Dijkstra took %lums\n", milliseconds);
    }

    if (!noInputModeEnabled) {
        printf ("Enter target node id... ");
        NodeID targetNodeId;
        if (scanf(" %u", &targetNodeId) == 1 && targetNodeId <= getLargestNodeID ()) {
            const int distance = getDistance (targetNodeId);
            printf ("Distance from %u to %u is %u\n", startNodeId, targetNodeId, distance);
        } else {
            printf ("Invalid input.\n");
        }
    }
}

/**
 * @brief Prints the help message detailing the command usage and options.
 * 
 * This function displays a comprehensive help message detailing the various
 * command-line switches, their syntax, and the associated notes. It covers 
 * both required and optional switches, providing examples and specific 
 * details on each switch's usage.
 * 
 * @note This help message is intended to be displayed when the user invokes 
 * the program with the `-help` option or when invalid arguments are provided.
 */
static void printHelp (void) {
    printf("Usage: program_name [OPTION...]\n\n");

    printf("Required Switches:\n");
    printf("  -graph <graph_file_path>\n");
    printf("    Specifies the location of the graph representing the map.\n");
    printf("    Example: -graph /path/to/graph/file\n");
    printf("    Note: This switch must always come before -socket, -que, -lon, -lat, -s switches.\n\n");

    printf("Optional Switches:\n");
    printf("  1. Socket Mode\n");
    printf("     -socket <port_number>\n");
    printf("       Allows the backend to listen to incoming requests from the frontend on the specified port.\n");
    printf("       This switch is exclusive and no other switches should follow it.\n");
    printf("       Example: -socket 8080\n\n");

    printf("  2. Route Query Mode\n");
    printf("     -que <path_to_que_file>\n");
    printf("       Specifies the location of the file that contains source and destination node pairs for route calculations.\n");
    printf("       Example: -que /path/to/que/file\n");
    printf("       Note: This switch can be combined with others mentioned below except for -socket.\n\n");

    printf("  3. Benchmark Mode\n");
    printf("     -benchmark\n");
    printf("       Activates the benchmark mode which assesses the performance of all preceding operations.\n");
    printf("       Example: -benchmark\n");
    printf("       Note: To benchmark the map loader, use this switch before -graph.\n\n");

    printf("  4. Nearest Node Identification Mode\n");
    printf("     -lon <longitude_value> -lat <latitude_value>\n");
    printf("       Provides longitude and latitude coordinates. The system will identify and return the closest node to the provided coordinates.\n");
    printf("       Example: -lon 9.098 -lat 48.746\n\n");

    printf("  5. One-To-All Test Mode\n");
    printf("     -s <source_node_id>\n");
    printf("       Initiates the one-to-all Dijkstra algorithm starting from the specified source node. Once the process concludes, \n");
    printf("       the system will prompt the user to input a destination node. It will then return the distance between the \n");
    printf("       source and destination nodes based on the previously executed one-to-all operation.\n");
    printf("       Example: -s 638394\n\n");

    printf("Usage Notes:\n");
    printf("  1. The command line arguments are evaluated in the order they are present in.\n");
    printf("  2. The -graph switch is mandatory as the first switch for every operation with the exception of -benchmark.\n");
    printf("     All other switches are optional and are utilized based on the operation mode desired by the user.\n");
    printf("  3. The -socket switch should not be combined with any other switches. If provided, it should be the last switch in the command.\n");
}

/**
 * @brief Displays the usage instruction for the given option.
 *
 * Based on the provided option ID, this function prints out the
 * correct usage for that option. If the option ID is not recognized,
 * no message is printed.
 *
 * @param optionId The ID of the option for which usage instructions should be displayed.
 */
static void printOptionUsage (const int optionId) {
    switch (optionId)
    {
        case GRAPH_OPTION_ID:
            printf("Usage: -graph <graph_file_path>\n");
            break;
        case SOCKET_OPTION_ID:
            printf("Usage: -socket <port_number>\n");
            break;
        case QUE_OPTION_ID:
            printf("Usage: -que <path_to_que_file>\n");
            break;
        case LAT_OPTION_ID:
            printf("Usage: -lat <latitude_value>\n");
            break;
        case LON_OPTION_ID:
            printf("Usage: -lon <longitude_value>\n");
            break;
        case SOURCE_OPTION_ID:
            printf("Usage: -s <source_node_id>\n");
            break;
        default:
            break;
    }
}