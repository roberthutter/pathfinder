/**
 * @author Ramon Floor
 * @date 11/02/2023
*/
#ifndef __GRAPH_LOADER_H__
#define __GRAPH_LOADER_H__

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include "../data_structures.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Loads a graph from a file and constructs a corresponding Graph data structure.
 *
 * The function reads the total number of nodes and edges from the specified file,
 * allocates memory for the graph components (nodes, edges, mapNodes), and uses threads
 * to concurrently read edges and nodes from the file. If successful, it updates the
 * Graph structure and returns a pointer to the loaded graph. Memory is freed in case of failure.
 *
 * @param fileName The name of the file containing graph information.
 * @return A pointer to the loaded Graph structure, or NULL if loading fails.
 */
Graph *loadGraphFromFile (const char *const fileName);

#ifdef __cplusplus
}
#endif

#endif