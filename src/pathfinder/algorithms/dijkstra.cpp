#include "dijkstra.h"

static inline void initializeNodeCosts(const Graph graph);
static inline void saveNodeInfo(const Graph graph);
static inline void saveRouteInfo (const Graph graph, const NodeID destination);
static void *dijkstraHelper (void*);

typedef KNHeap<Cost, NodeID> KN_Node_Heap;

static pthread_t helperThread;
static bool helperThreadOnline = false;
static volatile bool killHelperThread = false;
static Semaphore prepareDijkstraSemaphore, doDijkstraSemaphore;
static KN_Node_Heap knHeap(COST_INFINITY + 1u, 0u);

bool initializeDijkstra(const Graph *const graph) {
    bool successful = false;
    //queue = (INode_Heap*) Quad_Node_Heap_construct (graph->numberOfNodes * 1.05f);
    if (pthread_create (&helperThread, NULL, dijkstraHelper, (void*) graph) == 0) {
        Semaphore_init (&doDijkstraSemaphore, 0u);
        Semaphore_init (&prepareDijkstraSemaphore, 0u);
        helperThreadOnline = true;
        successful = true;
    }
    return successful;
}

void destroyDijkstra (void) {
    if (helperThreadOnline) {
        Semaphore_wait (&doDijkstraSemaphore);
        killHelperThread = true;
        Semaphore_post (&prepareDijkstraSemaphore);
    }
}

bool doDijkstra (const Graph graph, const NodeID startNode, const NodeID destination, const DijkstraMode mode) {
    assert (helperThreadOnline && startNode < graph.numberOfNodes && destination < graph.numberOfNodes);

    Semaphore_wait (&doDijkstraSemaphore);

    setCost (&graph.nodes[startNode], 0u);
    knHeap.insert (0u, startNode);
    while (__builtin_expect (!knHeap.isEmpty(), 1L)) {
        Cost currentCost;
        NodeID currentNodeID;
        knHeap.deleteMin(&currentCost, &currentNodeID);
        const Node currentNode = graph.nodes[currentNodeID];

        if (getDeletedFlag (currentNode) == 0u) {
            if (__builtin_expect (mode == ONE_TO_ONE && currentNodeID == destination, 0L)) {
                break;
            }
            setDeletedFlag (&graph.nodes[currentNodeID], 1u);

            const uint8_t edgeCount = getEdgeCount (currentNode);
            if (__builtin_expect (edgeCount > 0u, 1L)) {
                Edge *pEdge = &graph.edges[getEdgesIndex (currentNode)];
                Edge edge = *pEdge;
                NodeID targetID = edge.target;
                Node target = graph.nodes[targetID];
                if (getDeletedFlag (target) == 0u && currentCost + edge.cost < getCost (target)) {
                    setCost (&graph.nodes[targetID], currentCost + edge.cost);
                    knHeap.insert (currentCost + edge.cost, targetID);
                    if (mode == ONE_TO_ONE) {
                        graph.mapNodes[targetID].previous = currentNodeID;
                    }
                }

                if (__builtin_expect (edgeCount > 1u, 1L)) {
                    pEdge++;
                    edge = *pEdge;
                    targetID = edge.target;
                    target = graph.nodes[targetID];
                    if (getDeletedFlag (target) == 0u && currentCost + edge.cost < getCost (target)) {
                        setCost (&graph.nodes[targetID], currentCost + edge.cost);
                        knHeap.insert (currentCost + edge.cost, targetID);
                        if (mode == ONE_TO_ONE) {
                            graph.mapNodes[targetID].previous = currentNodeID;
                        }
                    }

                    for (uint8_t i = 0u; __builtin_expect (i < edgeCount - 2u, 0L); i++) {
                        pEdge++;
                        edge = *pEdge;
                        targetID = edge.target;
                        target = graph.nodes[targetID];
                        if (getDeletedFlag (target) == 0u && currentCost + edge.cost < getCost (target)) {
                            setCost (&graph.nodes[targetID], currentCost + edge.cost);
                            knHeap.insert (currentCost + edge.cost, targetID);
                            if (mode == ONE_TO_ONE) {
                                graph.mapNodes[targetID].previous = currentNodeID;
                            }
                        }
                    }
                }
            }
        }
    }
    bool routeFound = true;
    if (mode == ONE_TO_ONE) {
        routeFound = !isNodeUnreachable (graph.nodes[destination]);
        if (routeFound) {
            saveRouteInfo (graph, destination);
        }
    }
    Semaphore_post (&prepareDijkstraSemaphore);
    return routeFound;
}

/**
 * @brief Helper function for managing Dijkstra's algorithm execution in a thread.
 *
 * This function is meant to be used as the starting point for a helper thread
 * in the context of running Dijkstra's algorithm. It performs the necessary
 * preparations before the algorithm starts and ensures that the data remains
 * consistent after the algorithm has run. Specifically, it copies the cost
 * values to distance values for each node and then re-initializes all nodes and
 * the priority queue for future use.
 *
 * @param threadArg A pointer to user-defined data which should be cast to the appropriate
 *            type to use within the function. This is typically a pointer to a
 *            struct containing the graph and priority queue.
 */
static void *dijkstraHelper (void *threadArg) {
    const Graph graph = *(Graph*) threadArg;

    while (!killHelperThread) {
        while (!knHeap.isEmpty()) {
            Cost cost;
            NodeID nodeID;
            knHeap.deleteMin (&cost, &nodeID);
        }

        saveNodeInfo(graph);
        initializeNodeCosts (graph);

        Semaphore_post (&doDijkstraSemaphore);
        Semaphore_wait (&prepareDijkstraSemaphore);
    }
    Semaphore_destroy (&doDijkstraSemaphore);
    Semaphore_destroy (&prepareDijkstraSemaphore);
    killHelperThread = false;
    helperThreadOnline = false;
    return NULL;
}

/**
 * @brief Copies Dijkstra's algorithm route results to mapNodes structure.
 *
 * This function transfers the results of the point-to-point Dijkstra
 * algorithm from the graph's internal structures to the mapNodes for
 * external use. The information copied includes the distance of each node
 * along the route from the start node to the destination node.
 *
 * @param graph Graph structure from which the Dijkstra
 *              results are to be copied.
 * @note This function should only be called after the Dijkstra algorithm
 *       has been executed to ensure the results are valid and up-to-date.
 */
static inline void saveRouteInfo (const Graph graph, const NodeID destination) {
    MapNode *currentMapNode = &graph.mapNodes[destination];
    NodeID currentNodeID = destination;
    bool done = false;
    while (__builtin_expect (!done, 1L)) {
        currentMapNode->distance = getCost (graph.nodes[currentNodeID]);
        if (__builtin_expect (currentMapNode->distance == 0u, 0L)) {
            done = true;
        } else {
            currentNodeID = currentMapNode->previous;
            currentMapNode = &graph.mapNodes[currentNodeID];
        }
    }
}

/**
 * @brief Copies Dijkstra's algorithm results to the mapNodes structure.
 *
 * This function transfers the results of the Dijkstra algorithm from
 * the graph's internal structures to the mapNodes for external use.
 * The information copied includes the distance of each node from the
 * start node.
 *
 * @param graph Graph structure from which the Dijkstra
 *              results are to be copied.
 * @note This function should only be called after the Dijkstra algorithm
 *       has been executed to ensure the results are valid and up-to-date.
 */
static inline void saveNodeInfo (const Graph graph) {
    for (NodeID i = 0u; i < graph.numberOfNodes; i++) {
        if (i + 16u < graph.numberOfNodes) {
            __builtin_prefetch (graph.nodes + i + 16u, 0u, 2u);
        }
        graph.mapNodes[i].distance = getCost (graph.nodes[i]);
    }
}

/**
 * @brief Initializes all node costs in the graph to INFINITY.
 *
 * @param graph Graph whose node costs are to be initialized.
 */
static inline void initializeNodeCosts (const Graph graph) {
    for (NodeID i = 0u; i < graph.numberOfNodes; i++) {
        if (i + 16u < graph.numberOfNodes) {
            __builtin_prefetch (graph.nodes + i + 16u, 0u, 2u);
        }
        initializeNode (&graph.nodes[i]);
    }
}
