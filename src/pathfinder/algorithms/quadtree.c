#include "quadtree.h"

struct QuadTree {
    Coordinate bottomLeft;
    Coordinate topRight;
    QTNode *nodes;
    QuadTree *northwest;
    QuadTree *northeast;
    QuadTree *southeast;
    QuadTree *southwest;
    NodeID nodesInTree;
    bool isLeaf;
    bool isFull;
};

typedef enum {
    NORTHWEST,
    NORTHEAST,
    SOUTHEAST,
    SOUTHWEST
} Quadrant;

typedef struct {
    Quadrant quadrant;
    Coordinate bottomLeft;
    Coordinate topRight;
} Quadrant_t;

typedef struct {
    Quadrant_t nw;
    Quadrant_t ne;
    Quadrant_t se;
    Quadrant_t sw;
} SubdivisionQuadrants;

typedef struct {
    Coordinate bottomLeft;
    Coordinate topRight;
} Boundary;

static QuadTree emptyTree;
static Boundary failureBoundary;

static void unloadQuadTreeRecursive(QuadTree *quadtree);
static Boundary findBoundaries(const MapNode *const mapNodes, const NodeID numberOfNodes);
static void insert(QuadTree *const tree, const QTNode point);
static bool inBoundary(const QuadTree *const tree, const Coordinate point);
static Quadrant_t whatQuadrant(const QuadTree tree, const Coordinate point);
static SubdivisionQuadrants getSubdivisionQuadrants(const QuadTree tree);
static void subdivide(QuadTree *const tree);
static QTNode createNode(const Coordinate coordinate, const NodeID nodeID);
static QuadTree *createQuadTree(Coordinate bottomLeft, Coordinate topRight);
static void closestNodeHelper(const QuadTree *node, QTNode **const bestNode, double *const bestDistance, const Coordinate query);
static double euclideanDistance(const Coordinate coord1, const Coordinate coord2);
static bool intersects(const Coordinate query, const double radius, const Coordinate bottomLeft, const Coordinate topRight);

QuadTree *loadQuadTree(const Graph *graph) {
    assert(graph != NULL);
    QuadTree *returnValue = NULL;
    Boundary boundaries = findBoundaries(graph->mapNodes, graph->numberOfNodes);
    if(&boundaries != &failureBoundary) {
        QuadTree *qt = createQuadTree(boundaries.bottomLeft, boundaries.topRight);
        if(qt != NULL) {
            for(NodeID i = 0; i < graph->numberOfNodes; i++) {
            insert(qt, createNode(graph->mapNodes[i].coordinate, i));
        }
        returnValue = qt;  
        }    
    }
    return returnValue;
}

void unloadQuadTree(QuadTree *quadtree) {
    unloadQuadTreeRecursive(quadtree);
}

static void unloadQuadTreeRecursive(QuadTree *quadtree) {
    if(quadtree->isLeaf) {
        free(quadtree->nodes);
    }
    unloadQuadTree(quadtree->northwest);
    unloadQuadTree(quadtree->northeast);
    unloadQuadTree(quadtree->southwest);
    unloadQuadTree(quadtree->southeast);
    free(quadtree->northwest);
    free(quadtree->northeast);
    free(quadtree->southwest);
    free(quadtree->southeast);
}

/**
 * @brief Finds the geographical boundaries of a map based on the given mapNodes.
 *
 * This function takes an array of mapNodes and the number of nodes as input. If the number
 * of nodes is greater than 0, it iterates through the mapNodes to determine the minimum and
 * maximum latitude and longitude values. The function then constructs a Boundary structure
 * representing the boundaries of the map, expanded by adding a margin of 1 unit to each side.
 * If no nodes are provided or an error occurs, a predefined failureBoundary is returned.
 *
 * @param mapNodes Array of MapNode structures representing nodes on the map.
 * @param numberOfNodes The number of nodes in the map.
 * @return A Boundary structure representing the geographical boundaries of the map,
 *         or a predefined failureBoundary in case of an error.
 */
static Boundary findBoundaries(const MapNode *const mapNodes, const NodeID numberOfNodes) {
    Boundary returnValue = failureBoundary;
    if(numberOfNodes > 0) {
        double minLat = mapNodes[0].coordinate.latitude;
        double minLon = mapNodes[0].coordinate.longitude;
        double maxLat = mapNodes[0].coordinate.latitude;
        double maxLon = mapNodes[0].coordinate.longitude;

        for(NodeID i = 1; i < numberOfNodes; i++) {
            double tmpLat = mapNodes[i].coordinate.latitude;
            double tmpLon = mapNodes[i].coordinate.longitude;

            if(tmpLat < minLat) {
                minLat = tmpLat;
            }

            if(tmpLat > maxLat) {
                maxLat = tmpLat;
            }

            if(tmpLon < minLon) {
                minLon = tmpLon;
            }

            if(tmpLon > maxLon) {
                maxLon = tmpLon;
            }
        }

        Coordinate bottomLeft = {minLon-1, minLat-1};
        Coordinate topRight = {maxLon+1, maxLat+1};

        Boundary boundaries = {bottomLeft, topRight};
        returnValue = boundaries;
    } 

    return returnValue;
}

/**
 * @brief Inserts a point into the QuadTree structure.
 *
 * This function inserts a point represented by the QTNode structure into the QuadTree.
 * It checks if the point is within the boundaries of the QuadTree, and if so, it proceeds
 * with the insertion. If the QuadTree is a leaf and not full, the point is added directly.
 * If the QuadTree is a leaf and full, it is subdivided. The point is then inserted into the
 * appropriate quadrant based on its position. The process is recursive until a suitable leaf
 * node is found for insertion.
 *
 * @param tree The QuadTree structure.
 * @param point The QTNode representing the point to be inserted.
 */
static void insert(QuadTree *const tree, const QTNode point) {
    if(inBoundary(tree, point.point)) {
        if(tree->isLeaf) {
            if(!tree->isFull) {
                tree->nodes[tree->nodesInTree] = point;
                tree->nodesInTree++;
                if(tree->nodesInTree == NODES_PER_TREE) {
                    tree->isFull = true;
                }
                return;
            } else {
                subdivide(tree);
            }
        }

        Quadrant_t quadrant = whatQuadrant(*tree, point.point);

        switch (quadrant.quadrant)
        {
        case NORTHWEST:
            insert(tree->northwest, point);
            break;
        
        case NORTHEAST:
            insert(tree->northeast, point);
            break;

        case SOUTHEAST:
            insert(tree->southeast, point);
            break;

        case SOUTHWEST:
            insert(tree->southwest, point);
            break;
        }
    }
}

/**
 * @brief Checks if a coordinate point is within the boundaries of a QuadTree.
 *
 * This function determines whether the given coordinate point is within the boundaries
 * defined by the QuadTree. It compares the latitude and longitude of the point with the
 * bottom-left and top-right corners of the QuadTree. Returns true if the point is within
 * the boundaries; otherwise, returns false.
 *
 * @param tree The QuadTree structure defining the boundaries.
 * @param point The coordinate point to be checked.
 * @return true if the point is within the QuadTree boundaries, false otherwise.
 */
static bool inBoundary(const QuadTree *const tree, const Coordinate point) {
    bool bottomLatDiff = tree->bottomLeft.latitude <= point.latitude;
    bool topLatDiff = tree->topRight.latitude >= point.latitude;
    bool bottomLonDiff = tree->bottomLeft.longitude <= point.longitude;
    bool topLonDiff = tree->topRight.longitude >= point.longitude;

    return bottomLatDiff && topLatDiff && bottomLonDiff && topLonDiff;
}

/**
 * @brief Determines the quadrant of a coordinate point within a QuadTree.
 *
 * This function takes a QuadTree and a coordinate point as input and determines the quadrant
 * in which the point lies within that QuadTree. It calculates the middle latitude and longitude
 * of the QuadTree's boundaries and compares them with the given point to determine the quadrant.
 * The result is a Quadrant_t structure indicating the quadrant along with its updated boundaries.
 *
 * @param tree The QuadTree structure defining the boundaries.
 * @param point The coordinate point for which the quadrant is determined.
 * @return A Quadrant_t structure representing the determined quadrant and its boundaries.
 */
static Quadrant_t whatQuadrant(const QuadTree tree, const Coordinate point) {
    Quadrant_t returnValue;
    double middleLon = ((tree.bottomLeft.longitude + tree.topRight.longitude) / 2.0);
    double middleLat = ((tree.bottomLeft.latitude + tree.topRight.latitude) / 2.0);

    if(point.latitude >= middleLat) {
        if(point.longitude <= middleLon) {
            Coordinate bottomLeft = {tree.bottomLeft.longitude, middleLat};
            Coordinate topRight = {middleLon, tree.topRight.latitude};
            Quadrant_t q = {NORTHWEST, bottomLeft, topRight};
            returnValue = q;
        } else {
            Coordinate bottomLeft = {middleLon, middleLat};
            Coordinate topRight = tree.topRight;
            Quadrant_t q = {NORTHEAST, bottomLeft, topRight};
            returnValue = q;
        }
    } else {
        if(point.longitude <= middleLon) {
            Coordinate bottomLeft = tree.bottomLeft;
            Coordinate topRight = {middleLon, middleLat};
            Quadrant_t q = {SOUTHWEST, bottomLeft, topRight};
            returnValue = q;
        } else {
            Coordinate bottomLeft = {middleLon, tree.bottomLeft.latitude};
            Coordinate topRight = {tree.topRight.longitude, middleLat};
            Quadrant_t q = {SOUTHEAST, bottomLeft, topRight};
            returnValue = q;
        }
    }
    return returnValue;
}

/**
 * @brief Calculates and returns the subdivision quadrants of a QuadTree.
 *
 * This function takes a QuadTree structure as input and calculates the subdivision quadrants
 * by finding the middle latitude and longitude of the QuadTree's boundaries. It then constructs
 * four Quadrant_t structures representing the Northwest (NW), Northeast (NE), Southwest (SW),
 * and Southeast (SE) quadrants with updated boundaries. The result is a SubdivisionQuadrants
 * structure containing these four quadrants.
 *
 * @param tree The QuadTree structure for which subdivision quadrants are calculated.
 * @return A SubdivisionQuadrants structure containing the Northwest, Northeast, Southeast, and Southwest quadrants.
 */
static SubdivisionQuadrants getSubdivisionQuadrants(const QuadTree tree) {
    double middleLon = (tree.bottomLeft.longitude + tree.topRight.longitude) / 2.0;
    double middleLat = (tree.bottomLeft.latitude + tree.topRight.latitude) / 2.0;

    Coordinate bottomLeftNW = {tree.bottomLeft.longitude, middleLat};
    Coordinate topRightNW = {middleLon, tree.topRight.latitude};
    Quadrant_t northwest = {NORTHWEST, bottomLeftNW, topRightNW};

    Coordinate bottomLeftNE = {middleLon, middleLat};
    Coordinate topRightNE = tree.topRight;
    Quadrant_t northeast = {NORTHEAST, bottomLeftNE, topRightNE};

    Coordinate bottomLeftSW = tree.bottomLeft;
    Coordinate topRightSW = {middleLon, middleLat};
    Quadrant_t southwest = {SOUTHWEST, bottomLeftSW, topRightSW};

    Coordinate bottomLeftSE = {middleLon, tree.bottomLeft.latitude};
    Coordinate topRightSE = {tree.topRight.longitude, middleLat};
    Quadrant_t southeast = {SOUTHEAST, bottomLeftSE, topRightSE};

    SubdivisionQuadrants sq = {northwest, northeast, southeast, southwest};

    return sq;
}

/**
 * @brief Subdivides a QuadTree into four quadrants and redistributes its nodes.
 *
 * This function marks the QuadTree as non-leaf, calculates the subdivision quadrants,
 * and creates new QuadTree nodes for each quadrant. It redistributes the existing nodes
 * of the parent QuadTree into the appropriate child nodes based on their positions.
 * The function also updates the full status of each child node if it reaches the maximum
 * number of nodes. Finally, it frees the memory allocated for the original nodes array.
 *
 * @param tree The QuadTree structure to be subdivided.
 */
static void subdivide(QuadTree *const tree) {
    tree->isLeaf = false;

    SubdivisionQuadrants sq = getSubdivisionQuadrants(*tree);

    tree->northwest = createQuadTree(sq.nw.bottomLeft, sq.nw.topRight);
    tree->northeast = createQuadTree(sq.ne.bottomLeft, sq.ne.topRight);
    tree->southeast = createQuadTree(sq.se.bottomLeft, sq.se.topRight);
    tree->southwest = createQuadTree(sq.sw.bottomLeft, sq.sw.topRight);

    for(NodeID i = 0; i < tree->nodesInTree; i++) {
        Quadrant_t q = whatQuadrant(*tree, tree->nodes[i].point);
    
        switch (q.quadrant)
        {
        case NORTHWEST:
            tree->northwest->nodes[tree->northwest->nodesInTree] = tree->nodes[i];
            tree->northwest->nodesInTree++;
            break;
        
        case NORTHEAST:
            tree->northeast->nodes[tree->northeast->nodesInTree] = tree->nodes[i];
            tree->northeast->nodesInTree++;
            break;

        case SOUTHEAST:
            tree->southeast->nodes[tree->southeast->nodesInTree] = tree->nodes[i];
            tree->southeast->nodesInTree++;
            break;

        case SOUTHWEST:
            tree->southwest->nodes[tree->southwest->nodesInTree] = tree->nodes[i];
            tree->southwest->nodesInTree++;
            break;
        }
    }
    if(tree->northwest->nodesInTree == NODES_PER_TREE) {
        tree->northwest->isFull = true;
    } else if(tree->northeast->nodesInTree == NODES_PER_TREE) {
        tree->northeast->isFull = true;
    } else if(tree->southeast->nodesInTree == NODES_PER_TREE) {
        tree->southeast->isFull = true;
    } else if(tree->southwest->nodesInTree == NODES_PER_TREE) {
        tree->southwest->isFull = true;
    }
    free (tree->nodes);
}

/**
 * @brief Checks if a circular region, defined by a center and radius, intersects with a rectangular region.
 *
 * This function determines whether a circular region, specified by a center coordinate (query)
 * and a radius, intersects with a rectangular region defined by its bottom-left and top-right coordinates.
 * It calculates the relative positions of the circular and rectangular regions and returns true if there is an intersection,
 * indicating that the circular region partially or completely overlaps with the rectangular region.
 *
 * @param query The center coordinate of the circular region.
 * @param radius The radius of the circular region.
 * @param bottomLeft The bottom-left coordinate of the rectangular region.
 * @param topRight The top-right coordinate of the rectangular region.
 * @return true if the circular region intersects with the rectangular region, false otherwise.
 */
static bool intersects(const Coordinate query, const double radius, const Coordinate bottomLeft, const Coordinate topRight) {
    bool leftOfQuadrant = query.longitude + radius < bottomLeft.longitude;
    bool rightOfQuadrant = query.longitude - radius > topRight.longitude;
    bool aboveQuadrant = query.latitude - radius > topRight.latitude;
    bool belowQuadrant = query.latitude + radius < bottomLeft.latitude;

    return !(leftOfQuadrant || rightOfQuadrant || aboveQuadrant || belowQuadrant);

}

static QTNode createNode(const Coordinate coordinate, const NodeID nodeID) {
    QTNode node;
    node.point = coordinate;
    node.nodeID = nodeID;
    return node;
}

static QuadTree *createQuadTree(const Coordinate bottomLeft, const Coordinate topRight) {
    QuadTree *returnValue = NULL;
    QuadTree *qt = malloc(sizeof(QuadTree));

    if(qt != NULL) {
        qt->nodesInTree = 0;
        qt->nodes = malloc(sizeof(QTNode) * NODES_PER_TREE);
        qt->isLeaf = true;
        qt->isFull = false;
        qt->bottomLeft = bottomLeft;
        qt->topRight = topRight;
        qt->northwest = &emptyTree;
        qt->northeast = &emptyTree;
        qt->southeast = &emptyTree;
        qt->southwest = &emptyTree;
        returnValue = qt;
    }
    return returnValue;
}

/**
 * @brief Finds the closest node to a given query point in a QuadTree.
 *
 * This function searches for the closest node to the specified query point within a QuadTree.
 * It initializes variables to track the best node and its distance, then calls a helper function
 * to recursively explore the QuadTree and update the best node based on proximity to the query point.
 *
 * @param query The coordinate point for which the closest node is sought.
 * @param quadtree The QuadTree structure to search for the closest node.
 * @return A pointer to the closest QTNode or NULL if the QuadTree is empty.
 */
QTNode *closestNode(const Coordinate query, const QuadTree *const quadtree) {
    QTNode *bestNode = NULL;
    double bestDistance = __DBL_MAX__;

    if (quadtree != NULL) {
        closestNodeHelper(quadtree, &bestNode, &bestDistance, query);
    }

    return bestNode;
}

/**
 * @brief Helper function to find the closest node to a query point in a QuadTree.
 *
 * This function is a helper for the closestNode function and recursively explores a QuadTree node.
 * It checks for intersection between the current QuadTree node and a circular region centered around
 * the query point. If an intersection is found, the function traverses the QuadTree nodes to find the
 * closest node to the query point. If the QuadTree node is a leaf, it iterates through its points to
 * update the bestNode and bestDistance based on the proximity to the query point.
 *
 * @param node The current QuadTree node being explored.
 * @param bestNode A pointer to the current best QTNode.
 * @param bestDistance A pointer to the current best distance to the query point.
 * @param query The coordinate point for which the closest node is sought.
 */
static void closestNodeHelper(const QuadTree *const node, QTNode **const bestNode, double *const bestDistance, const Coordinate query) {
    if (node == NULL) {
        return;
    }
    bool intersect = intersects(query, *bestDistance, node->bottomLeft, node->topRight);
    if(intersect) {
        if (node->isLeaf) {
            // Node is a leaf, iterate through the points and find the closest one
            for (NodeID i = 0; i < node->nodesInTree; i++) {
                double currentDistance = euclideanDistance(query, node->nodes[i].point);

                // Update bestNode and bestDistance if the current point is closer
                if (currentDistance < *bestDistance) {
                *bestNode = &(node->nodes[i]);
                *bestDistance = currentDistance;
            }
        }
        } else {
            closestNodeHelper(node->northwest, bestNode, bestDistance, query);
            closestNodeHelper(node->northeast, bestNode, bestDistance, query);
            closestNodeHelper(node->southeast, bestNode, bestDistance, query);
            closestNodeHelper(node->southwest, bestNode, bestDistance, query);
        }
    }
}

static double euclideanDistance(const Coordinate coord1, const Coordinate coord2) {
    return sqrt(pow(coord2.latitude - coord1.latitude, 2) + pow(coord2.longitude - coord1.longitude, 2));
}

