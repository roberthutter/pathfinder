/**
 * @author Ramon Floor
*/

#ifndef __QUADTREE_H__
#define __QUADTREE_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include "../data_structures.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct QuadTree QuadTree;

/**
 * @brief Constructs a QuadTree based on the given Graph data structure.
 *
 * This function takes a pointer to a Graph struct and constructs a QuadTree
 * representation based on the spatial information present in the graph. The QuadTree
 * is a hierarchical data structure that partitions 2D space into regions to
 * efficiently organize and query spatial data.
 *
 * @param graph A pointer to the Graph struct containing spatial information.
 * @return A pointer to the constructed QuadTree.
 */
QuadTree *loadQuadTree(const Graph *graph);

/**
 * @brief Unloads and frees the memory associated with a QuadTree.
 *
 * This function takes a pointer to a QuadTree structure and recursively unloads and frees
 * the memory associated with it, including all its child nodes and stored data. After calling
 * this function, the QuadTree is left in an empty and uninitialized state.
 *
 * @param quadtree A pointer to the QuadTree structure to be unloaded and freed.
 */
void unloadQuadTree(QuadTree *quadtree);

/**
 * @brief Finds the closest node to the given query coordinate.
 *
 * This function takes a QuadTree pointer and a query coordinate. It traverses
 * the QuadTree to find the QuadTree node that is closest to the specified
 * coordinate in 2D space.
 *
 * @param query The coordinate for which to find the closest QuadTree node.
 * @param quadtree A pointer to the QuadTree from which to search.
 * @return A pointer to the QuadTree node that is closest to the query coordinate.
 */
QTNode *closestNode(const Coordinate query, const QuadTree *const quadtree);

#ifdef __cplusplus
}
#endif

#endif