/**
 * @author Robert Hutter
 * @date 12/09/2023
*/
#ifndef __DIJKSTRA_H__
#define __DIJKSTRA_H__

#include <assert.h>
#include <stdio.h>
#include <pthread.h>
#include "cross_semaphore.h"
#include "../data_structures.h"
#include "../heaps/k_ary_node_heap.h"
#include "../heaps/quad_node_heap.h"
#include "../spq/knheap.cpp"

typedef enum DijkstraMode {
    ONE_TO_ALL,
    ONE_TO_ONE
} DijkstraMode;

/**
 * @brief Initializes the algorithms library for the Dijkstra algorithm.
 *
 * This function sets up the necessary data structures for running the Dijkstra
 * algorithm on a given graph. It initializes the priority queue required for
 * the algorithm's execution and spawns a helper thread dedicated to cleanup
 * tasks post-execution.
 *
 * @param[in] graph A pointer to the Graph structure on which Dijkstra's
 * algorithm will be run.
 * @return Returns `true` if the initialization was successful, `false` otherwise.
 */
bool initializeDijkstra(const Graph *const graph);

/**
 * @brief Frees all resources associated with the Dijkstra algorithm.
 *
 * This function is responsible for cleaning up and releasing all resources
 * allocated for the Dijkstra algorithm's execution. It ensures that the
 * priority queue is properly freed and that any helper threads created for
 * cleanup are terminated and cleaned up.
 *
 * @note This function should be called after the Dijkstra algorithm has been
 * run and the results are no longer needed, to prevent memory leaks and
 * dangling threads.
 */
void destroyDijkstra(void);

/**
 * @brief Executes Dijkstra's algorithm on the provided graph.
 *
 * This function runs Dijkstra's algorithm starting from the specified startNode. It operates in two modes:
 * ONE_TO_ALL or ONE_TO_ONE. In ONE_TO_ALL mode, it computes the shortest paths from the startNode to all
 * other nodes in the graph, and the destination parameter is ignored. In ONE_TO_ONE mode, it computes the 
 * shortest path between the startNode and the destination node specified.
 *
 * The results of the operation, including the computed shortest distances, are stored in the distance values
 * of the MapNodes associated with the graph and can be retrieved after the operation completes.
 *
 * @param graph The graph structure on which Dijkstra's algorithm will be executed.
 * @param startNode The starting node ID for the path computation.
 * @param destination The destination node ID for the path computation if mode is ONE_TO_ONE.
 * @param mode The mode of operation for Dijkstra's algorithm: ONE_TO_ALL or ONE_TO_ONE.
 * @note This function requires that `initializeDijkstra` has been successfully called before invocation.
 */
bool doDijkstra (const Graph graph, const NodeID startNode, const NodeID destination, const DijkstraMode mode);

#endif