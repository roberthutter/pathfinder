/**
 * @file semaphore_wrapper.h
 * @brief A cross-platform semaphore wrapper for POSIX and Apple systems.
 *
 * This file provides an abstraction over semaphores by wrapping the POSIX semaphore
 * functions on Unix-like systems and Grand Central Dispatch (GCD) semaphores on Apple platforms.
 * 
 * @author Robert Hutter
 * @date 11/04/2023
 * @link https://stackoverflow.com/questions/27736618/why-are-sem-init-sem-getvalue-sem-destroy-deprecated-on-mac-os-x-and-w
 */
#ifndef __CROSS_SEMAPHORE_H__
#define __CROSS_SEMAPHORE_H__

#ifdef __APPLE__
    #include <dispatch/dispatch.h>
#else
    #include <errno.h>
    #include <semaphore.h>
#endif
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief The Semaphore structure abstracts a semaphore for cross-platform compatibility.
 */
typedef struct Semaphore {
    #ifdef __APPLE__
        dispatch_semaphore_t    sem;
    #else
        sem_t                   sem;
    #endif
} Semaphore;

/**
 * @brief Initializes a semaphore with a given initial value.
 *
 * @param semaphore Pointer to the semaphore to initialize.
 * @param value The initial count for the semaphore object.
 * @return 0 if the semaphore was successfully created, or -1 otherwise.
 */
static inline int Semaphore_init (Semaphore* const semaphore, const uint32_t value) {
    #ifdef __APPLE__
        dispatch_semaphore_t* sem = &semaphore->sem;

        *sem = dispatch_semaphore_create(value);
        return (*sem != NULL) ? 0 : -1;
    #else
        return sem_init(&semaphore->sem, 0, value);
    #endif
}

/**
 * @brief Decrements (locks) the semaphore. If the semaphore's value is greater than zero,
 * the decrement proceeds, and the function returns, immediately. If the semaphore
 * currently has the value zero, the call blocks until either it becomes possible to
 * perform the decrement (i.e., the semaphore value rises above zero), or a signal handler
 * interrupts the call.
 *
 * @param semaphore Pointer to the semaphore to decrement.
 */
static inline void Semaphore_wait (Semaphore* const semaphore) {
    #ifdef __APPLE__
        dispatch_semaphore_wait(semaphore->sem, DISPATCH_TIME_FOREVER);
    #else
        int r;

        do {
                r = sem_wait(&semaphore->sem);
        } while (r == -1 && errno == EINTR);
    #endif
}

/**
 * @brief Increments (unlocks) the semaphore. If the semaphore's value consequently becomes
 * greater than zero, another process or thread blocked in a Semaphore_wait() call will be woken
 * up and proceed to lock the semaphore.
 *
 * @param semaphore Pointer to the semaphore to increment.
 */
static inline void Semaphore_post (Semaphore* const semaphore) {
    #ifdef __APPLE__
        dispatch_semaphore_signal(semaphore->sem);
    #else
        sem_post(&semaphore->sem);
    #endif
}

/**
 * @brief Destroys the semaphore object, freeing resources it may hold.
 *
 * @param semaphore Pointer to the semaphore to destroy.
 */
static inline void Semaphore_destroy (Semaphore* const semaphore) {
    #ifdef __APPLE__

    #else
        sem_destroy (&semaphore->sem);
    #endif
}

#ifdef __cplusplus
}
#endif

#endif