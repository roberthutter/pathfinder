#include "graph_loader.h"

static Graph graph;
static Node *nodes;
static Edge *edges;
static MapNode *mapNodes;
static NodeID numberOfNodes;
static EdgeID numberOfEdges;
static const int THREAD_SUCCESS = 0;
static const int THREAD_FAILURE = 1;

static const char *graphFile;

static void* readEdges(void *arg);
static void* readNodes(void *arg);
static bool readNumberOfNodesAndEdges();

Graph *loadGraphFromFile (const char *const fileName) {
    graphFile = fileName;
    Graph* returnValue = NULL;

    if(access(graphFile, R_OK) == 0) {
        /*Read total number of nodes and edges from file*/
        bool status = readNumberOfNodesAndEdges();

        if(status) {
            /*Allocate memory for Graph datastructure components*/
            edges = malloc(numberOfEdges * sizeof(Edge));
            if(edges != NULL) {
                nodes = malloc(numberOfNodes * sizeof(Node));
                if(nodes != NULL) {
                    mapNodes = malloc(numberOfNodes * sizeof(MapNode));
                    if(mapNodes != NULL) {
                        /*Build graph datastructure using threads*/
                        pthread_t readEdgeThread;
                        pthread_t readNodeThread;

                        pthread_create(&readEdgeThread, NULL, readEdges, NULL);
                        pthread_create(&readNodeThread, NULL, readNodes, NULL);

                        int *readEdgesStatus, *readNodeStatus;
                        pthread_join(readEdgeThread, (void**) &readEdgesStatus);
                        pthread_join(readNodeThread, (void**) &readNodeStatus);

                        if(*readEdgesStatus == THREAD_SUCCESS && *readNodeStatus == THREAD_SUCCESS) {
                            graph.numberOfNodes = numberOfNodes;
                            graph.numberOfEdges = numberOfEdges;
                            graph.nodes = nodes;
                            graph.edges = edges;
                            graph.mapNodes = mapNodes;

                            returnValue = &graph;
                        } else {
                            free(nodes);
                            free(edges);
                            free(mapNodes);
                        }
                    } else {
                        free(edges);
                        free(nodes);
                    }
                } else {
                    free(edges);
                }
            }
        }
    }
    return returnValue;
}

/**
 * @brief Reads edge information from a file and populates the edges array.
 *
 * This function is intended to be executed in a separate thread. It opens the file
 * specified by `graphFile`, skips the initial lines, and reads edge information line by line.
 * For each edge, it extracts source, target, and cost values, updates relevant node information,
 * and populates the edges array. If successful, it returns THREAD_SUCCESS; otherwise, it returns THREAD_FAILURE.
 *
 * @param arg Unused argument (required for pthread_create).
 * @return THREAD_SUCCESS if edges are successfully read, THREAD_FAILURE otherwise.
 */
static void* readEdges(void *arg) {
    void *returnValue = (void*) &THREAD_FAILURE;

    FILE *filepointer = fopen(graphFile, "r");
    if(filepointer != NULL) {
        NodeID startAtLine = numberOfNodes + 8;

        char line[65];
        char *first;

        for(NodeID i = 1; i < startAtLine; i++) {
            first = fgets(line, 65, filepointer);
        }

        char *endpointer;
        int64_t previousSource = -1;
        returnValue = (void*) &THREAD_SUCCESS;

        for(EdgeID currentEdge = 0; currentEdge < numberOfEdges; currentEdge++) {
            first = fgets(line, 65, filepointer);
            if(first != NULL) {
                const NodeID src = (NodeID) strtol(line, &endpointer, 10);
                const NodeID trgt = (NodeID) strtol(endpointer, &endpointer, 10);
                const Cost cst = (Cost) strtol(endpointer, &endpointer, 10);

                if(previousSource != src) {
                    setEdgesIndex (&nodes[src], currentEdge);
                    previousSource = src;
                }

                setEdgeCount (&nodes[src], getEdgeCount (nodes[src]) + 1);

                const Edge edge = {trgt, cst};
                edges[currentEdge] = edge;
            } else {
                returnValue = (void*) &THREAD_FAILURE;
                break;
            }
        }
        fclose(filepointer);
    }

    return returnValue;
}

/**
 * @brief Reads node information from a file and populates the nodes and mapNodes arrays.
 *
 * This function is intended to be executed in a separate thread. It opens the file
 * specified by `graphFile`, skips the initial lines, and reads node information line by line.
 * For each node, it extracts node ID and extracts additional information
 * such as the second node ID, latitude, and longitude. It updates the mapNodes array with coordinate
 * information. If successful, it returns THREAD_SUCCESS; otherwise, it returns THREAD_FAILURE.
 *
 * @param arg Unused argument (required for pthread_create).
 * @return THREAD_SUCCESS if nodes are successfully read, THREAD_FAILURE otherwise.
 */
static void* readNodes(void *arg) {
    void *returnValue = (void*) &THREAD_FAILURE;
    
    FILE *filepointer = fopen(graphFile, "r");
    if(filepointer != NULL) {
        char line[65];
        char *first;
        char *endpointer;

        /*Skip first 7 lines*/
        for(NodeID i = 1; i < 8; i++) {
            first = fgets(line, 65, filepointer);
        }

        for(NodeID currentNode = 0; currentNode < numberOfNodes; currentNode++) {
            returnValue = (void*) &THREAD_FAILURE;

            first = fgets(line, 65, filepointer);
            if(first != NULL) {
                const NodeID nodeID = (NodeID) strtol(line, &endpointer, 10);
                if (currentNode == nodeID) {
                    const NodeID nodeID2 = (NodeID) strtol(endpointer, &endpointer, 10);
                    const double latitude = strtod(endpointer, &endpointer);
                    const double longitude = strtod(endpointer, &endpointer);

                    Coordinate coordinate;
                    coordinate.latitude = latitude;
                    coordinate.longitude = longitude;
                    mapNodes[currentNode].coordinate = coordinate;
                    returnValue = (void*) &THREAD_SUCCESS;
                }
            }
            
            if (returnValue == &THREAD_FAILURE) {
                break;
            }
        }
        fclose(filepointer);
    }
    return returnValue;
}

/**
 * @brief Reads the total number of nodes and edges from a file.
 *
 * This function opens the file specified by `graphFile`, skips the initial lines, and reads
 * the number of nodes and edges. It updates the global variables numberOfNodes and numberOfEdges.
 * If successful, it returns true; otherwise, it returns false.
 *
 * @return true if the number of nodes and edges are successfully read, false otherwise.
 */
static bool readNumberOfNodesAndEdges() {
    bool returnValue = false;

    FILE *filepointer = fopen(graphFile, "r");
    if(filepointer != NULL) {
        char line[65];
        char *first;

        for(NodeID i = 1; i < 6; i++) {
            first = fgets(line, 65, filepointer);
        }

        first = fgets(line, 65, filepointer);
        if(first != NULL) {
            numberOfNodes = (NodeID) strtol(line, NULL, 10);
            first = fgets(line, 65, filepointer);
            if(first != NULL) {
                numberOfEdges = (NodeID) strtol(line, NULL, 10);
                returnValue = true;
            } else {
                returnValue = false;
            }
        } else {
            returnValue = false;
        }
        fclose(filepointer);
    }
    return returnValue;
}