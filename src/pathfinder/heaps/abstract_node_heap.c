#include "abstract_node_heap.h"

static inline Cost_Compare_Result Costs_Comparator (const NodeEntry a, const NodeEntry b) {
    Cost_Compare_Result result;
    if (a.cost > b.cost) {
        result = COST_GREATER;
    } else if (a.cost < b.cost) {
        result = COST_LESS;
    } else {
        result = COST_EQUAL;
    }
    return result;
}

static inline void swap (const Abstract_Node_Heap heap, const uint32_t indexA, const uint32_t indexB) {
    const NodeEntry temp = heap.heap[indexA];
    heap.heap[indexA] = heap.heap[indexB];
    heap.heap[indexB] = temp;
}

static inline bool add (Heap_Self self, const Cost cost, const NodeID nodeID) {
    Abstract_Node_Heap *pHeap = (Abstract_Node_Heap*) self;
    const Abstract_Node_Heap heap = *pHeap;
    bool successful = false;
    
    if (__builtin_expect (heap.size < heap.capacity, 0u)) {
        heap.heap[heap.size] = (NodeEntry) {cost, nodeID};
        heap.shiftUp (pHeap, heap.size);
        pHeap->size++;
        successful = true;
    }
    return successful;
}

static NodeID remove (Heap_Self self) {
    Abstract_Node_Heap *pHeap = (Abstract_Node_Heap*) self;
    Abstract_Node_Heap heap = *pHeap;
    NodeID result = UINT32_MAX;

    if (__builtin_expect (heap.size, 1u)) {
        result = heap.heap[0u].nodeID;
        heap.size--;
        pHeap->size = heap.size;
        heap.heap[0u] = heap.heap[heap.size];
        heap.shiftDown (pHeap, 0u);
    }
    return result;
}

static inline void removeAll (Heap_Self self) {
    ((Abstract_Node_Heap*) self)->size = 0u;
}

static inline bool isNotEmpty (Heap_Self self) {
    return ((Abstract_Node_Heap*) self)->size > 0u;
}

static inline bool isEmpty (Heap_Self self) {
    return ((Abstract_Node_Heap*) self)->size == 0u;
}

Abstract_Node_Heap Abstract_Node_Heap_initialize (void) {
    Abstract_Node_Heap nodeHeap;
    nodeHeap.interface.add = add;
    nodeHeap.interface.remove = remove;
    nodeHeap.interface.removeAll = removeAll;
    nodeHeap.interface.isEmpty = isEmpty;
    nodeHeap.interface.isNotEmpty = isNotEmpty;
    nodeHeap.Costs_Comparator = Costs_Comparator;
    nodeHeap.heap = NULL;
    nodeHeap.size = 0u;
    nodeHeap.capacity = 0u;
    nodeHeap.swap = swap;
    return nodeHeap;
}