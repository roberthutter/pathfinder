/**
 * @file inode_heap.h
 * @brief Interface for managing Node Heaps.
 *
 * This file defines the `INode_Heap` structure and associated functions for managing heaps of Nodes.
 * It includes operations such as adding, removing, and modifying nodes within the heap, 
 * as well as functions for checking the state of the heap.
 * 
 * @author Robert Hutter
 * @date 11/19/2023
 * Documentation inspired by ChatGPT.
 */
#ifndef __INODE_HEAP_H__
#define __INODE_HEAP_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include "../data_structures.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct INode_Heap INode_Heap;
typedef INode_Heap *const Heap_Self;

struct INode_Heap {
    /**
     * @brief Add a node to the heap.
     * 
     * @param self A pointer to the heap instance.
     * @param node The node to be added to the heap.
     * @return true on successful addition, false otherwise.
     */
    bool (*add)(Heap_Self, const Cost, const NodeID);

    /**
     * @brief Remove and return the top node from the heap.
     * 
     * @param self A pointer to the heap instance.
     * @return The removed node, or UINT32_MAX if the heap is empty.
     */
    NodeID (*remove)(Heap_Self);

    /**
     * @brief Remove all nodes from the heap.
     * 
     * @param self A pointer to the heap instance.
     */
    void (*removeAll)(Heap_Self);

    /**
     * @brief Check if the heap is not empty.
     * 
     * @param self A pointer to the heap instance.
     * @return true if the heap is not empty, false otherwise.
     */
    bool (*isNotEmpty)(Heap_Self);

    /**
     * @brief Check if the heap is empty.
     * 
     * @param self A pointer to the heap instance.
     * @return true if the heap is empty, false otherwise.
     */
    bool (*isEmpty)(Heap_Self);

    /**
     * @brief Destroy the heap and free up resources.
     * 
     * @param self A pointer to the heap instance.
     */
    void (*destroy)(Heap_Self);
};

#ifdef __cplusplus
}
#endif

#endif /* __INODE_HEAP_H__ */