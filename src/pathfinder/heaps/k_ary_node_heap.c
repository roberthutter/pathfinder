#include "k_ary_node_heap.h"

static inline void shiftUp (Abstract_Node_Heap *const pNodeHeap, const uint32_t index) {
    const Abstract_Node_Heap nodeHeap = *pNodeHeap;
    const K_Ary_Node_Heap kHeap = *(K_Ary_Node_Heap*) pNodeHeap;
    uint32_t currentIndex = index;
    uint32_t parentIndex = (index - 1) / kHeap.k;
    while (currentIndex > 0u && nodeHeap.Costs_Comparator (nodeHeap.heap[parentIndex], nodeHeap.heap[currentIndex]) == COST_GREATER) {
        nodeHeap.swap (nodeHeap, parentIndex, currentIndex);
        currentIndex = parentIndex;
        parentIndex = (currentIndex - 1) / kHeap.k;
    }
}

static inline void shiftDown (Abstract_Node_Heap *const pNodeHeap, const uint32_t index) {
    const Abstract_Node_Heap nodeHeap = *pNodeHeap;
    const K_Ary_Node_Heap kHeap = *(K_Ary_Node_Heap*) pNodeHeap;
    uint32_t currentIndex = index;
    uint32_t firstChild = index * kHeap.k + 1u;
    
    while (firstChild < nodeHeap.size) {
        unsigned int nextTarget = firstChild;

        if (kHeap.k == 4) {
            if (firstChild + 1u < nodeHeap.size) {
                if (nodeHeap.Costs_Comparator (nodeHeap.heap[firstChild + 1], nodeHeap.heap[nextTarget]) == COST_LESS) {
                    nextTarget = firstChild + 1u;
                }
                if (firstChild + 2u < nodeHeap.size) {
                    if (nodeHeap.Costs_Comparator (nodeHeap.heap[firstChild + 2], nodeHeap.heap[nextTarget]) == COST_LESS) {
                        nextTarget = firstChild + 2u;
                    }
                    if (firstChild + 3u < nodeHeap.size) {
                        if (nodeHeap.Costs_Comparator (nodeHeap.heap[firstChild + 3], nodeHeap.heap[nextTarget]) == COST_LESS) {
                            nextTarget = firstChild + 3u;
                        }
                    }
                }
            }
        } else {
            for (uint8_t i = 1u; i < kHeap.k && firstChild + i < nodeHeap.size; i++) {
                if (nodeHeap.Costs_Comparator (nodeHeap.heap[firstChild + i], nodeHeap.heap[nextTarget]) == COST_LESS) {
                    nextTarget = firstChild + i;
                }
            }
        }

        if (nodeHeap.Costs_Comparator (nodeHeap.heap[currentIndex], nodeHeap.heap[nextTarget]) == COST_GREATER) {
            nodeHeap.swap (nodeHeap, nextTarget, currentIndex);
            currentIndex = nextTarget;
            firstChild = nextTarget * kHeap.k + 1u;
        } else {
            break;
        }
    }
}

static inline void destroy (Heap_Self self) {
    if (self != NULL) {
        K_Ary_Node_Heap *nodeHeap = (K_Ary_Node_Heap*) self;
        free (nodeHeap->nodeHeap.heap);
    }
}

K_Ary_Node_Heap *K_Ary_Node_Heap_construct (const uint8_t k, const uint32_t size) {
    K_Ary_Node_Heap *pHeap = malloc (sizeof (K_Ary_Node_Heap));
    K_Ary_Node_Heap *result = NULL;
    if (pHeap != NULL) {
        if (__builtin_expect (K_Ary_Node_Heap_initialize (pHeap, k, size), 1)) {
            result = pHeap;
        }
        free (pHeap);
    }
    return result;
}

bool K_Ary_Node_Heap_initialize (K_Ary_Node_Heap *const pHeap, const uint8_t k, const uint32_t size) {
    assert (pHeap);
    Abstract_Node_Heap *const nodeHeap = &pHeap->nodeHeap;
    bool successful = false;
    *nodeHeap = Abstract_Node_Heap_initialize ();
    nodeHeap->heap = malloc (sizeof (NodeEntry) * size);
    if (nodeHeap->heap != NULL) {
        nodeHeap->interface.destroy = destroy;
        nodeHeap->shiftUp = shiftUp;
        nodeHeap->shiftDown = shiftDown;
        nodeHeap->capacity = size;
        pHeap->k = k;
        successful = true;
    }
    return successful;
}