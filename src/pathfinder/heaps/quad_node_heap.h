/**
 * @file quad_node_heap.h
 * @brief Implementation of a Quad Node Heap.
 *
 * This file provides an implementation of a quad node heap, extending the abstract node heap 
 * defined in abstract_node_heap.h. In a quad node heap, each node has exactly four children.
 * This file includes methods for constructing and initializing the heap, tailored to the 
 * specific structure of a quad node heap.
 * 
 * @author Robert Hutter
 * @date 11/19/2023
 * Documentation inspired by ChatGPT.
 */
#ifndef __QUAD_NODE_HEAP_H__
#define __QUAD_NODE_HEAP_H__

#include "abstract_node_heap.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    Abstract_Node_Heap nodeHeap; // Base abstract node heap, extended for quad node functionality.
} Quad_Node_Heap;

/**
 * @brief Construct a new Quad_Node_Heap object.
 * 
 * @param size Initial capacity of the heap.
 * @return A pointer to the newly constructed Quad_Node_Heap.
 * @note The user is responsible for destroying the heap instance by calling
 * 'Node_Heap_destroy' in 'inode_heap.h' as well as free on the reference returned
 * by this method.
 */
Quad_Node_Heap *Quad_Node_Heap_construct(const uint32_t);

/**
 * @brief Initialize a Quad_Node_Heap object.
 * 
 * @param heap Pointer to the Quad_Node_Heap to initialize.
 * @param size Initial capacity of the heap.
 * @return true if initialization is successful, false otherwise.
 * @note The user is responsible for destroying the heap instance by calling
 * 'Node_Heap_destroy' in 'inode_heap.h'.
 */
bool Quad_Node_Heap_initialize(Quad_Node_Heap *const, const uint32_t);

#ifdef __cplusplus
}
#endif

#endif /* __QUAD_NODE_HEAP_H__ */
