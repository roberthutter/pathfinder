/**
 * @file k_ary_node_heap.h
 * @brief Implementation of a k-ary Node Heap.
 *
 * This file provides an implementation of the abstract node heap described in abstract_node_heap.h.
 * The k-ary node heap is a specialized heap where every node has 'k' children.
 * It includes methods for constructing and initializing the heap, as well as the necessary 
 * implementations of the abstract methods defined in the base abstract heap.
 * 
 * @author Robert Hutter
 * @date 11/19/2023
 * Documentation inspired by ChatGPT.
 */
#ifndef __K_ARY_NODE_HEAP_H__
#define __K_ARY_NODE_HEAP_H__

#include "abstract_node_heap.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    Abstract_Node_Heap nodeHeap; // Base abstract node heap.
    uint8_t k;                   // Number of children for each node in the heap.
} K_Ary_Node_Heap;

/**
 * @brief Construct a new K_Ary_Node_Heap object.
 * 
 * @param k The number of children each node in the heap should have.
 * @param size Initial capacity of the heap.
 * @return A pointer to the newly constructed K_Ary_Node_Heap.
 * @note The user is responsible for destroying the heap instance by calling
 * 'Node_Heap_destroy' in 'inode_heap.h' as well as free on the reference returned
 * by this method.
 */
K_Ary_Node_Heap *K_Ary_Node_Heap_construct(const uint8_t, const uint32_t);

/**
 * @brief Initialize a K_Ary_Node_Heap object.
 * 
 * @param heap Pointer to the K_Ary_Node_Heap to initialize.
 * @param k The number of children each node in the heap should have.
 * @param size Initial capacity of the heap.
 * @return true if initialization is successful, false otherwise.
 * @note The user is responsible for destroying the heap instance by calling
 * 'Node_Heap_destroy' in 'inode_heap.h'.
 */
bool K_Ary_Node_Heap_initialize(K_Ary_Node_Heap *const, const uint8_t, const uint32_t);

#ifdef __cplusplus
}
#endif

#endif /* __K_ARY_NODE_HEAP_H__ */
