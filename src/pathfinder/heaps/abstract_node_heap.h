/**
 * @file abstract_node_heap.h
 * @brief Abstract interface for Node Heaps.
 *
 * This header file defines an abstract base for implementing Node Heaps. It extends the functionality
 * defined in inode_heap.h. This abstract heap is not meant to be used directly as it lacks implementation
 * of essential methods. It should be extended by concrete Node Heap implementations.
 * Extending classes must define the operations 'shiftDown', 'shiftUp', 'swap', 'Costs_Comparator', 
 * as well as 'destroy' from inode_heap. The initializer method returns an instance with all methods 
 * implemented by this class configured.
 * 
 * @author Robert Hutter
 * @date 11/19/2023
 * Documentation inspired by ChatGPT.
 */
#ifndef __ABSTRACT_NODE_HEAP_H__
#define __ABSTRACT_NODE_HEAP_H__

#include "inode_heap.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct Abstract_Node_Heap Abstract_Node_Heap;

typedef struct {
    Cost cost;
    NodeID nodeID;
} NodeEntry;

typedef enum {
    COST_GREATER,
    COST_EQUAL,
    COST_LESS
} Cost_Compare_Result;

/**
 * @brief Function pointer type for comparing two nodes based on their costs.
 * 
 * @param a First node to compare.
 * @param b Second node to compare.
 * @return Comparison result as Cost_Compare_Result enum.
 */
typedef Cost_Compare_Result (*Node_Comparator)(const NodeEntry, const NodeEntry);

struct Abstract_Node_Heap {
    INode_Heap interface;  // Interface inherited from INode_Heap.

    Node_Comparator Costs_Comparator; // Function to compare nodes based on costs.

    /**
     * @brief Shift down operation in the heap.
     * 
     * @param self Pointer to the Abstract_Node_Heap instance.
     * @param index Index at which the shift down operation starts.
     */
    void (*shiftDown)(Abstract_Node_Heap *const, const uint32_t);

    /**
     * @brief Shift up operation in the heap.
     * 
     * @param self Pointer to the Abstract_Node_Heap instance.
     * @param index Index at which the shift up operation starts.
     */
    void (*shiftUp)(Abstract_Node_Heap *const, const uint32_t);

    /**
     * @brief Swap two nodes in the heap.
     * 
     * The default implementation sets the 'heapIndex' member
     * of the node at index1 to that of index2. The 'heapIndex'
     * member of the second node is not modified by default.
     * 
     * @param self Abstract_Node_Heap instance.
     * @param index1 Index of the first node to swap.
     * @param index2 Index of the second node to swap.
     */
    void (*swap)(const Abstract_Node_Heap, const uint32_t, const uint32_t);

    NodeEntry* heap; // Pointer to the array of nodes (heap).
    uint32_t size; // Current size of the heap.
    uint32_t capacity; // Maximum capacity of the heap.
};

/**
 * @brief Initialize and return a new Abstract_Node_Heap instance.
 * 
 * @return An initialized Abstract_Node_Heap instance.
 */
Abstract_Node_Heap Abstract_Node_Heap_initialize(void);

#ifdef __cplusplus
}
#endif

#endif /* __ABSTRACT_NODE_HEAP_H__ */
