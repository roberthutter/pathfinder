#include "quad_node_heap.h"

/**
 * \brief Checks to see if the node a has a lower cost then that of node b.
 * 
 * @param a Node a
 * @param b Node b
 * @return true if the cost of node a is less then that of node b.
*/
static inline bool isNodeACloser (const NodeEntry a, const NodeEntry b) {
    return a.cost < b.cost;
}

static inline void shiftUp (Abstract_Node_Heap *const pNodeHeap, const uint32_t index) {
    const Abstract_Node_Heap nodeHeap = *pNodeHeap;
    uint32_t currentIndex = index;
    uint32_t parentIndex = (index - 1u) >> 2u;
    while (currentIndex > 0u && isNodeACloser (nodeHeap.heap[currentIndex], nodeHeap.heap[parentIndex])) {
        nodeHeap.swap (nodeHeap, parentIndex, currentIndex);
        currentIndex = parentIndex;
        parentIndex = (currentIndex - 1u) >> 2u;
    }
}

static inline void shiftDown (Abstract_Node_Heap *const pNodeHeap, const uint32_t index) {
    const Abstract_Node_Heap nodeHeap = *pNodeHeap;
    uint32_t currentIndex = index;
    uint32_t firstChild = (index << 2u) + 1u;
    
    while (__builtin_expect (firstChild < nodeHeap.size, 1L)) {
        uint32_t nextTarget = firstChild;

        if (firstChild + 1u < nodeHeap.size) {
            if (isNodeACloser (nodeHeap.heap[firstChild + 1u], nodeHeap.heap[nextTarget])) {
                nextTarget = firstChild + 1u;
            }

            if (firstChild + 2u < nodeHeap.size) {
                if (isNodeACloser (nodeHeap.heap[firstChild + 2u], nodeHeap.heap[nextTarget])) {
                    nextTarget = firstChild + 2u;
                }

                if (firstChild + 3u < nodeHeap.size) {
                    if (isNodeACloser (nodeHeap.heap[firstChild + 3u], nodeHeap.heap[nextTarget])) {
                        nextTarget = firstChild + 3u;
                    }
                }
            }
        }

        if (isNodeACloser (nodeHeap.heap[nextTarget], nodeHeap.heap[currentIndex])) {
            nodeHeap.swap (nodeHeap, nextTarget, currentIndex);
            currentIndex = nextTarget;
            firstChild = (nextTarget << 2u) + 1u;
        } else {
            break;
        }
    }
}

static inline void destroy (Heap_Self self) {
    if (self != NULL) {
        Quad_Node_Heap *nodeHeap = (Quad_Node_Heap*) self;
        free (nodeHeap->nodeHeap.heap);
    }
}

Quad_Node_Heap *Quad_Node_Heap_construct (const uint32_t size) {
    Quad_Node_Heap *pHeap = malloc (sizeof (Quad_Node_Heap));
    Quad_Node_Heap *result = NULL;
    if (pHeap != NULL) {
        if (__builtin_expect (Quad_Node_Heap_initialize (pHeap, size), 1L)) {
            result = pHeap;
        } else {
            free (pHeap);
        }
    }
    return result;
}

bool Quad_Node_Heap_initialize (Quad_Node_Heap *const pHeap, const uint32_t size) {
    assert (pHeap);
    Abstract_Node_Heap *const nodeHeap = &pHeap->nodeHeap;
    bool successful = false;
    *nodeHeap = Abstract_Node_Heap_initialize ();
    nodeHeap->heap = malloc (sizeof (NodeEntry) * size);
    if (nodeHeap->heap != NULL) {
        nodeHeap->interface.destroy = destroy;
        nodeHeap->shiftUp = shiftUp;
        nodeHeap->shiftDown = shiftDown;
        nodeHeap->capacity = size;
        successful = true;
    }
    return successful;
}