/**
 * \file pathfinder.h
 * \brief Interface for the Pathfinder Backend Software.
 *
 * This file provides the main interface to the Pathfinder backend system,
 * enabling users to load map graphs, calculate routes, and run various 
 * other operations related to map navigation. The system supports operations 
 * like nearest point identification, one-to-one and one-to-all route computations.
 *
 * \author Robert Hutter
 * \date 10/27/2023
 * 
 * \warning The library is not thread-safe and should only be called from one thread.
 */
#ifndef __PATHFINDER_H__
#define __PATHFINDER_H__

#include <assert.h>
#include <stdint.h>
#include <stdbool.h>
#include "data_structures.h"
#include "algorithms/dijkstra.h"
#include "algorithms/graph_loader.h"
#include "algorithms/quadtree.h"

/** Version number of the Pathfinder library. */
#define PATHFINDER_VERSION "v1.0.1"

/**
 * @brief Constructs a Coordinate object from given latitude and longitude.
 * 
 * This function takes a latitude and longitude in double precision and 
 * returns a Coordinate object representing that location.
 *
 * @param latitude The latitude of the coordinate.
 * @param longitude The longitude of the coordinate.
 * @return A Coordinate object representing the given latitude and longitude.
 */
Coordinate coordinateOf (const double latitude, const double longitude);

/**
 * @brief Attempts to load a graph from a specified file.
 * 
 * This function tries to read and load a graph from the file with the provided filename.
 * The function returns true if the loading process succeeds without any issues. Otherwise,
 * it returns false indicating that the graph could not be loaded from the specified file.
 *
 * @param graphFile The path to the file containing the graph data.
 * @return true if the graph is successfully loaded, false otherwise.
 */
bool loadGraph (const char *const graphFile);

/**
 * @brief Frees all memory allocated to the graph data structure.
 *
 * This function is responsible for deallocating all memory resources
 * associated with the graph. This includes freeing the
 * nodes, edges, and any other dynamically allocated structures that
 * make up the graph.
 */
void unloadGraph(void);

/**
 * @brief Sets up the data structure needed for completing closest node identification operations.
 * 
 * This function sets up the data structure needed to complete closest node identification
 * operations. Requires that the graph has been loaded using loadGraph().
 *
 * @pre The function `loadGraph` must have been successfully executed before calling this function.
 * @return True if the closest node data structure is successfully set up, false otherwise.
*/
bool setupClosestNodeDataStructure (void);

/**
 * @brief Executes Dijkstra's algorithm in a one-to-all node mode from a specified starting point.
 * 
 * This function runs Dijkstra's algorithm starting from the node with given node ID.
 * It calculates the shortest paths from this node to all other nodes in the graph.
 * Requires that the graph and related data structures have been successfully initialised.
 *
 * @param source Node ID of the starting node for Dijkstra's algorithm.
 * @pre isFullyInitialised()
 */
void doOneToAllDijkstra(const NodeID source);

/**
 * @brief Executes Dijkstra's algorithm in a point-to-point node mode from a specified starting point
 * to the specified destination.
 * 
 * Runs Dijkstra's algorithm starting from the node with the given source node ID.
 * It calculates the shortest path from this node to all other nodes until it reaches the
 * destination node. Requires that the graph and related data structures have been successfully
 * initialised and that both node IDs are not greater then the largest node ID.
 * 
 * @param source Node ID of the starting node for Dijkstra's algorithm.
 * @param destination Node ID of the destination node for Dijkstra's algorithm.
 * @return true if Dijkstra's algorithm found a route between the two points. False
 * if no route could be found.
 * @pre isFullyInitialised()
*/
bool doPointToPointDijkstra (const NodeID source, const NodeID destination);

/**
 * @brief Retrieves the shortest distance to a specified node.
 * 
 * This function returns the shortest distance to the specified destination node based on 
 * the results from a previous execution of Dijkstra's algorithm in one-to-all mode. 
 * It is mandatory that the one-to-all Dijkstra algorithm has been successfully run 
 * before invoking this function.
 *
 * @param destination The ID of the destination node to retrieve the distance for.
 * @return The shortest distance to the specified node.
 * @pre The one-to-all Dijkstra algorithm must have been successfully executed before calling this function.
 */
Distance getDistance (const NodeID destination);

/**
 * @brief Retrieves the coordinate of the closest node to the given point.
 * 
 * This function finds and returns the coordinate of the node that is closest to 
 * the specified point based on the loaded graph data. Requires that the graph
 * and related data structures have been successfully initialised.
 *
 * @param point The coordinate for which the closest node should be determined.
 * @return A Coordinate object representing the location of the nearest node to the given point.
 * @pre isFullyInitialised()
 */
Coordinate getClosestNodeCoordinate (const Coordinate point);

/**
 * @brief Retrieves the node ID of the closest node to the given point.
 * 
 * This function finds and returns the coordinate of the node that is closest to 
 * the specified point based on the loaded graph data. Requires that the graph
 * and related data structures have been successfully initialised.
 *
 * @param point The coordinate for which the closest node should be determined.
 * @return The node ID of the nearest node to the given point.
 * @pre isFullyInitialised()
 */
NodeID getClosestNode (const Coordinate point);

/**
 * @brief Returns the node ID of the last node in the graph.
 * 
 * Returns the largest node ID of this graph. Use this function to 
 * validate node IDs before passing them to other functions of this
 * package. Ensures that any node ID smaller then the one returned
 * by this function is a valid node ID.
 * 
 * @return Largest node ID in the graph.
*/
NodeID getLargestNodeID (void);

/**
 * \brief Checks if the graph and related data structures are fully initialised.
 *
 * This function determines if the graph has been successfully loaded into memory 
 * and whether all necessary data structures for the Pathfinder Backend Software 
 * have been correctly initialised, specifically when loadGraph() and
 * setupClosestNodeDataStructure() were called successfully.
 *
 * \return \c true if the graph is loaded and all related data structures are 
 *         initialised. \c false otherwise.
 * \note This function should be called before attempting any operations that 
 *       require the graph or related data structures to be initialised.
 */
bool isFullyInitialised(void);

/**
 * \brief Returns the loaded graph.
 * 
 * Returns the currently loaded graph. Requires that the graph has been
 * successfully loaded prior to calling this method.
 * 
 * \return The currently loaded graph.
*/
Graph getGraph (void);

#endif