/**
 * @file pathfinder_server.h
 * @brief Interface of the Pathfinder backend HTTP server.
 *
 * This file provides the interface and necessary definitions for managing the 
 * Pathfinder HTTP server. It includes functionalities to initialize and start the server, 
 * handle client requests, and manage server operations. The file defines the server's port 
 * type, status enumerations, and the main function to start the server.
 *
 * @author Robert Hutter, Luca Schuett
 * @date 10/30/2023
 */
#ifndef __PATHFINDER_SERVER_H__
#define __PATHFINDER_SERVER_H__

#include "pathfinder.h"
#include "mongoose/mongoose.h"
#include <stdint.h>
#include <string.h>

typedef uint16_t PortNumber;

/**
 * \enum ServerStatus
 * \brief Enumeration representing the status of the Pathfinder backend HTTP server.
 *
 * This enumeration provides potential status values indicating the outcome 
 * of the backend server startup and operation. It's mainly used to identify 
 * the reason for termination or any encountered issues during server operation.
 *
 * \var ServerStatus::SUCCESSFULLY_TERMINATED
 * Indicates that the server started successfully and terminated without errors.
 *
 * \var ServerStatus::SOCKET_UNAVAILABLE
 * Indicates that the specified socket was not available during server startup.
 *
 * \var ServerStatus::UNKNOWN_ERROR
 * Represents any other issues or unforeseen errors during server startup or operation.
 */
typedef enum ServerStatus {
    SUCCESSFULLY_TERMINATED,
    SOCKET_UNAVAILABLE,
    UNKNOWN_ERROR
} ServerStatus;

/**
 * @brief Starts the Pathfinder HTTP server on the specified socket.
 *
 * Initializes and starts the Pathfinder HTTP server on the provided socket.
 * Before launching the server, it ensures the pathfinder library is fully initialized 
 * using the `isFullyInitialised()` from the `pathfinder.h` library. 
 * If the server starts without issues, it will continue to run until interrupted externally. 
 * If there's a failure during the server's startup, a corresponding error status is returned.
 *
 * @param socket Port number for the server to listen on.
 *
 * @return Returns a status reflecting the server's startup outcome. 
 *         Possible returns are:
 *         - `SUCCESSFULLY_TERMINATED`: Server started and ran without problems and concluded normally.
 *         - `SOCKET_UNAVAILABLE`: The chosen socket is either in use or not bindable.
 *         - `UNKNOWN_ERROR`: Raised if the system isn't fully initialized or encounters another unspecified error.
 * @note The function keeps the invoking thread blocked for an indefinite period once the server is up. 
 *       It's advised to execute this in a suitable environment considering this behavior.
 */
ServerStatus startPathfinderServer (const PortNumber socket);

#endif