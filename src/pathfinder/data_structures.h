/**
 * @file data_structures.h
 * @brief Header file for data structures used in the Pathfinder project.
 *
 * This file contains definitions of various data structures, including nodes, edges,
 * graphs, and others, as well as utility functions for manipulating these structures.
 * It provides essential building blocks for representing and processing graph data.
 *
 * @authors Ramon Floor, Robert Hutter
 * @date 11/02/2023
 */
#ifndef _DATA_STRUCTURES_H__
#define _DATA_STRUCTURES_H__

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/** @brief The maximum amount of nodes to be stored in a leaf node of the Quad tree.
 * 
 * Decreasing this number will result in faster execution of the get closest node
 * operations, however will increase the size of the Quad tree and with that the
 * programs memory requirement.
*/
#define NODES_PER_TREE (50000)

typedef uint32_t Distance;
typedef uint32_t Cost;
typedef uint32_t NodeID;
typedef uint32_t EdgeID;

typedef uint64_t Node;

/** @brief Represents the infinity cost value for node distance */
#define COST_INFINITY (0x1FFFFFF)

static const uint64_t EDGES_INDEX_MASK  = 0xFFFFFFE000000000;  // Mask for the first 27 bits
static const uint64_t EDGE_COUNT_MASK   = 0x0000001E00000000;  // Mask for the next 4 bits
static const uint64_t COST_MASK         = 0x00000001FFFFFF00;  // Mask for the next 25 bits
static const uint64_t DELETED_FLAG_MASK = 0x00000000000000FF;  // Mask for the last 8 bits

static const int EDGES_INDEX_SHIFT = 37;
static const int EDGE_COUNT_SHIFT = 33;
static const int COST_SHIFT = 8;
static const int DELETED_FLAG_SHIFT = 0;

/**
 * @brief Retrieves the deleted flag from the given node.
 *
 * @param node The node from which to retrieve the deleted flag.
 * @return The deleted flag as a uint8_t value.
 */
static inline uint8_t getDeletedFlag (const Node node) {
    return (uint8_t) node;
}

/**
 * @brief Retrieves the cost associated with the given node.
 *
 * @param node The node from which to retrieve the cost.
 * @return The cost as a Cost value.
 */
static inline Cost getCost (const Node node) {
    return (Cost)(((node) & COST_MASK) >> COST_SHIFT);
}

/**
 * @brief Retrieves the edge count from the given node.
 *
 * @param node The node from which to retrieve the edge count.
 * @return The edge count as a uint8_t value.
 */
static inline uint8_t getEdgeCount (const Node node) {
    return (uint8_t)(((node) & EDGE_COUNT_MASK) >> EDGE_COUNT_SHIFT);
}

/**
 * @brief Retrieves the edges index from the given node.
 *
 * @param node The node from which to retrieve the edges index.
 * @return The edges index as a uint32_t value.
 */
static inline uint32_t getEdgesIndex (const Node node) {
    return ((node) & EDGES_INDEX_MASK) >> EDGES_INDEX_SHIFT;
}

/**
 * @brief Sets the deleted flag for the given node.
 *
 * @param node A pointer to the node for which to set the deleted flag.
 * @param flag The value to set for the deleted flag.
 */
static inline void setDeletedFlag (Node *const node, const uint8_t flag) {
    *node = ((*node) & ~DELETED_FLAG_MASK) | ((uint64_t) flag);
}

/**
 * @brief Sets the cost for the given node.
 *
 * @param node A pointer to the node for which to set the cost.
 * @param cost The value to set for the cost.
 */
static inline void setCost (Node *const node, const Cost cost) {
    *node = ((*node) & ~COST_MASK) | ((uint64_t)(cost) << COST_SHIFT);
}

/**
 * @brief Sets the deleted flag and the cost for the given node.
 *
 * @param node A pointer to the node for which to set the deleted flag and cost.
 * @param flag The value to set for the deleted flag.
 * @param cost The value to set for the cost.
 */
static inline void setDeletedFlagAndCost (Node * const node, const uint8_t flag, const Cost cost) {
    Node localNode = ((*node) & ~DELETED_FLAG_MASK) | ((uint64_t) flag);
    *node = ((localNode) & ~COST_MASK) | ((uint64_t)(cost) << COST_SHIFT);
}

/**
 * @brief Sets the edge count for the given node.
 *
 * @param node A pointer to the node for which to set the edge count.
 * @param edgeCount The value to set for the edge count.
 */
static inline void setEdgeCount (Node *const node, const uint8_t edgeCount) {
    *node = ((*node) & ~EDGE_COUNT_MASK) | ((uint64_t)(edgeCount) << EDGE_COUNT_SHIFT);
}

/**
 * @brief Sets the edges index for the given node.
 *
 * @param node A pointer to the node for which to set the edges index.
 * @param index The value to set for the edges index.
 */
static inline void setEdgesIndex (Node *const node, const uint32_t index) {
    *node = ((*node) & ~EDGES_INDEX_MASK) | ((uint64_t)(index) << EDGES_INDEX_SHIFT);
}

/**
 * @brief Initializes the given node, setting its cost to maximum and clearing the deleted flag.
 *
 * @param node A pointer to the node to initialize.
 */
static inline void initializeNode (Node *const node) {
    *node = ((*node) | COST_MASK) & ~DELETED_FLAG_MASK;
}

/**
 * @brief Checks if the given node is unreachable based on its cost.
 *
 * @param node The node to check for reachability.
 * @return true if the node is unreachable, false otherwise.
 */
static inline bool isNodeUnreachable (const Node node) {
    return (node & COST_MASK) == COST_MASK;
}

/**
 * @brief Structure representing geographical coordinates with double precision.
 */
typedef struct {
    double longitude; /**< Longitude value of the coordinate. */
    double latitude;  /**< Latitude value of the coordinate. */
} Coordinate;

/**
 * @brief Structure representing an edge in a graph.
 */
typedef struct {
    NodeID target; /**< The target node ID of the edge. */
    Cost cost;     /**< The cost associated with traversing this edge. */
} Edge;

/**
 * @brief Structure representing a node in a map.
 */
typedef struct {
    Coordinate coordinate; /**< The geographical coordinates of the map node. */
    NodeID previous;       /**< The ID of the previous node in a path. */
    Distance distance;     /**< The distance from the start node in a path. */
} MapNode;

/**
 * @brief Structure representing a graph.
 */
typedef struct {
    Edge *edges;           /**< Pointer to an array of edges in the graph. */
    Node *nodes;           /**< Pointer to an array of nodes in the graph. */
    MapNode *mapNodes;     /**< Pointer to an array of map nodes in the graph. */
    uint32_t numberOfNodes; /**< The total number of nodes in the graph. */
    uint32_t numberOfEdges; /**< The total number of edges in the graph. */
} Graph;

/**
 * @brief Structure representing a quadtree node.
 */
typedef struct {
    Coordinate point; /**< The coordinate point of the quadtree node. */
    NodeID nodeID;    /**< The ID of the associated node. */
} QTNode;

#ifdef __cplusplus
}
#endif

#endif
