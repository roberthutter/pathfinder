#include "../pathfinder_server.h"

// Define HTTP status codes
static const int ALL_OK = 200;
static const int BAD_REQUEST = 400;

static char httpAddress [40] = "http://localhost:";
static const char *ROOT_DIR = "web_root";
static const char *JSON_HEADER = "Content-Type: application/json\r\n";

static const char *JSON_ROUTE_HEADER = "{\
	\"type\":\"FeatureCollection\",\
	\"features\":[\
	{\
	\"type\":\"Feature\",\
	\"properties\":{},\
	\"geometry\":{\
	\"coordinates\":[";

static const char *JSON_ROUTE_FOOTER = "],\
	\"type\":\"LineString\"\
	}\
	}\
	]\
	}";

// APID Endpoint Definitions
static const char *NODE_API_URI = "/api/node";
static const char *ROUTE_API_URI = "/api/route";

typedef enum RequestError {
	MISSING_ARGUMENT_ERROR,
	INVALID_ARGUMENT_ERROR,
	NO_ROUTE_FOUND_ERROR,
	RUNTIME_ERROR
} RequestError;

static void sendJSONError (struct mg_connection *const connection, const RequestError error);
static void processCallToNodeAPI (struct mg_connection *const connection, const struct mg_http_message *const httpMessage);
static void processCallToRouteAPI (struct mg_connection *const connection, const struct mg_http_message *const httpMessage);
static void compileAndSendRoute (struct mg_connection *const connection, const NodeID destination);
static size_t compileRoute(char **const messageBody, const size_t initialSize, const NodeID destination);
static size_t resizeString(char **const string, const size_t newSize);
static void httpEventHandler (struct mg_connection *const connection, int eventId, void *eventData, void *fnData);

/**
 * @brief Sends a JSON formatted error message over the given connection.
 *
 * This function constructs and sends a JSON response containing an error message 
 * based on the provided `RequestError` enum value. The error message is sent with 
 * a "Bad Request" HTTP status code.
 *
 * @param connection Pointer to the mg_connection structure representing the active connection.
 * @param error Enum value representing the type of request error.
 */
static void sendJSONError (struct mg_connection *connection, const RequestError error) {
	char errorName[40];
	switch (error) {
		case MISSING_ARGUMENT_ERROR:
			strcpy (errorName, "\"Missing argument(s)\"");
			break;
		
		case INVALID_ARGUMENT_ERROR:
			strcpy (errorName, "\"Invalid argument(s)\"");
			break;

		case NO_ROUTE_FOUND_ERROR:
			strcpy (errorName, "\"No route found\"");
			break;

		case RUNTIME_ERROR:
			strcpy (errorName, "\"Runtime error\"");
			break;
		
		default:
			// Unhandled cases are prohibited
			assert (false);
			return;
	}
	mg_http_reply (connection, BAD_REQUEST, JSON_HEADER, "{%m:%s}\n", MG_ESC ("error"), errorName);
}

/**
 * @brief Processes a call to the Node API endpoint.
 * 
 * This function is designed to handle requests to the Node API endpoint. It extracts the 
 * latitude and longitude from the body of the HTTP message (expected to be in JSON format).
 * If both latitude and longitude are valid, it proceeds with nearest node identification.
 * If either the latitude or longitude is invalid or missing, 
 * an appropriate JSON error response is sent using the `sendJSONError` function.
 *
 * @param connection Pointer to the mg_connection structure representing the active connection.
 * @param httpMessage Pointer to the mg_http_message structure containing details of the received HTTP request.
 */
static void processCallToNodeAPI (struct mg_connection *const connection, const struct mg_http_message *const httpMessage) {
	char *const latitudeString = mg_json_get_str (httpMessage->body, "$.latitude");
	char *const longitudeString = mg_json_get_str (httpMessage->body, "$.longitude");
	
	if (latitudeString && longitudeString) {
		char *latEndChar, *lonEndChar;
		const double latitude = strtod (latitudeString, &latEndChar);
		const double longitude = strtod (longitudeString, &lonEndChar);
		if (latEndChar != latitudeString && lonEndChar != longitudeString && *latEndChar == '\0' && *lonEndChar == '\0') {
			printf ("Received lat: %f, lon: %f\n", latitude, longitude);
			const Coordinate closestNodeCoord = getClosestNodeCoordinate (coordinateOf (latitude, longitude));
			mg_http_reply (connection, ALL_OK, JSON_HEADER, "{%m:%.16f,%m:%.16f}", MG_ESC ("latitude"), closestNodeCoord.latitude, MG_ESC ("longitude"), closestNodeCoord.longitude);
		} else {
			sendJSONError (connection, INVALID_ARGUMENT_ERROR);
		}
	} else {
		sendJSONError (connection, MISSING_ARGUMENT_ERROR);
	}

	free (latitudeString);
	free (longitudeString);
}

/**
 * @brief Processes a call to the Route API endpoint.
 * 
 * This function is designed to handle requests to the Route API endpoint. It extracts the 
 * start and end latitude and longitude from the body of the HTTP message (expected to be in JSON format).
 * If the start and end latitude and longitude are provided and valid, it proceeds with route calculation.
 * If any of the latitude or longitude values is invalid or missing, 
 * an appropriate JSON error response is sent using the `sendJSONError` function.
 *
 * @param connection Pointer to the mg_connection structure representing the active connection.
 * @param httpMessage Pointer to the mg_http_message structure containing details of the received HTTP request.
 */
static void processCallToRouteAPI (struct mg_connection *const connection, const struct mg_http_message *const httpMessage) {
	char *startLatitudeString = mg_json_get_str (httpMessage->body, "$.start_latitude");
	char *startLongitudeString = mg_json_get_str (httpMessage->body, "$.start_longitude");
	char *endLatitudeString = mg_json_get_str (httpMessage->body, "$.end_latitude");
	char *endLongitudeString = mg_json_get_str (httpMessage->body, "$.end_longitude");

	if (startLatitudeString && startLongitudeString && endLatitudeString && endLongitudeString) {
		char *startLatEndChar, *startLonEndChar, *endLatEndChar, *endLonEndChar;
		double startLatitude = strtod (startLatitudeString, &startLatEndChar);
		double startLongitude = strtod (startLongitudeString, &startLonEndChar);
		double endLatitude = strtod (endLatitudeString, &endLatEndChar);
		double endLongitude = strtod (endLongitudeString, &endLonEndChar);

		if (startLatEndChar != startLatitudeString && startLonEndChar != startLongitudeString &&
			endLatEndChar != endLatitudeString && endLonEndChar != endLongitudeString &&
			*startLatEndChar == '\0' && *startLonEndChar == '\0' &&
			*endLatEndChar == '\0' && *endLonEndChar == '\0'
		) {
			printf ("Received starting lat: %f, lon: %f, end lat: %f, lon: %f\n", startLatitude, startLongitude, endLatitude, endLongitude);
			const NodeID source = getClosestNode (coordinateOf (startLatitude, startLongitude));
			const NodeID destination = getClosestNode (coordinateOf (endLatitude, endLongitude));
			if (doPointToPointDijkstra (source, destination)) {
				compileAndSendRoute (connection, destination);
			} else {
				sendJSONError (connection, NO_ROUTE_FOUND_ERROR);
			}
		} else {
			sendJSONError (connection, INVALID_ARGUMENT_ERROR);
		}
	} else {
		sendJSONError (connection, MISSING_ARGUMENT_ERROR);
	}

	free (startLatitudeString);
	free (startLongitudeString);
	free (endLatitudeString);
	free (endLongitudeString);
}

/**
 * @brief Compiles the route to a specified node and sends it over an HTTP connection.
 *
 * This function first compiles the route to the given destination node and then sends
 * this information as an HTTP message over the provided connection. If the compilation of the
 * route fails for any reason, an error message is sent instead.
 *
 * @param connection A pointer to the `mg_connection` structure representing the HTTP connection
 *                   over which the message will be sent.
 * @param destination The ID of the destination node for which the route is to be compiled.
 */
static void compileAndSendRoute (struct mg_connection *const connection, const NodeID destination) {
	assert (connection);
	char *messageBody = (char*) malloc (sizeof (char) * 2097152);
	bool successful = false;
	if (messageBody != NULL) {
		memcpy (messageBody, JSON_ROUTE_HEADER, strlen (JSON_ROUTE_HEADER) + 1);
		size_t totalSize = compileRoute (&messageBody, 2097152, destination);
		if (totalSize > 0) {
			if (strlen (messageBody) + strlen (JSON_ROUTE_FOOTER) > totalSize - 1) {
				totalSize = resizeString (&messageBody, totalSize * 2);
			}
			if (totalSize) {
				strcat (messageBody, JSON_ROUTE_FOOTER);
				mg_http_reply (connection, ALL_OK, JSON_HEADER, "%s", messageBody);
				successful = true;
			}
		}
		free (messageBody);
	}
	if (!successful) {
		sendJSONError (connection, RUNTIME_ERROR);
	}
}

/**
 * @brief Compiles a JSON array of nodes along the route to the given destination node.
 *
 * This function constructs a JSON-formatted string representing the route to a specified
 * destination node. The route is represented as an array of nodes in JSON format and is
 * appended to the string pointed to by `messageBody`. The function returns the final size
 * of the message body string after the route has been compiled and added.
 *
 * @param messageBody A pointer to the pointer of the string where the JSON array will be appended.
 *                    This string should be dynamically allocated and large enough to hold the
 *                    initial content and the additional JSON data. The function may reallocate
 *                    this string if more space is needed.
 * @param initialSize The initial size of `messageBody` before the route is added.
 * @param destination The ID of the destination node for which the route is compiled.
 * @return The final size of the message body string after appending the JSON array. If the
 *         function fails (e.g., due to memory allocation issues), 0 is returned.
 */
static size_t compileRoute (char **const messageBody, const size_t initialSize, const NodeID destination) {
	assert (messageBody);
	size_t size = initialSize;
	char line[128];

	MapNode nextNode = getGraph().mapNodes[destination];
	bool done = false;
	while (!done) {
		if (nextNode.distance > 0) {
			snprintf (line, 128, "[%.16f,%.16f],", nextNode.coordinate.longitude, nextNode.coordinate.latitude);
		} else {
			snprintf (line, 128, "[%.16f,%.16f]", nextNode.coordinate.longitude, nextNode.coordinate.latitude);
			done = true;
		}
		if (strlen (line) + strlen (*messageBody) > size - 1) {
			size = resizeString (messageBody, size * 2);
			if (size == 0) {
				break;
			}
		}
		strcat (*messageBody, line);
		nextNode = getGraph().mapNodes[nextNode.previous];
	}
	return size;
}

/**
 * @brief Resizes a string to the specified new size.
 *
 * This function adjusts the size of the memory allocated for a string
 * pointed to by `string` to `newSize`. The function returns the 
 * size of the resized string.
 *
 * @param string A pointer to the pointer of the string to be resized. The pointer
 *               is modified to point to the newly allocated space.
 * @param newSize The new size for the string in bytes.
 * @return The size of the string after resizing. 0 if memory allocation fails.
 */
static size_t resizeString (char **const string, const size_t newSize) {
	assert (newSize > strlen (*string));
	*string = (char*) realloc (*string, newSize);
	return *string != NULL ? newSize : 0;
}

/**
 * @brief Handles HTTP events triggered by the mongoose server.
 * 
 * This function is responsible for handling incoming HTTP requests and directing
 * them to the appropriate processing functions based on the request URI. It can handle
 * requests to the Node API, Route API, or serve static files from the specified root directory.
 *
 * @param connection The active connection with the client.
 * @param eventId The event identifier, which specifies the type of event.
 * @param eventData Data associated with the event. It is cast to `mg_http_message` if the event is an HTTP message.
 * @param fnData User-defined data passed to the event handler.
 * 
 * @note This function assumes that the mongoose server is correctly set up and that 
 *       the connection is an active one. It is also expected to be called internally 
 *       by the mongoose event loop and not directly by user code.
 */
static void httpEventHandler (struct mg_connection *const connection, int eventId, void *eventData, void *fnData) {
	if (eventId == MG_EV_HTTP_MSG) {
		struct mg_http_message *const httpMessage = (struct mg_http_message*) eventData;

		if (mg_http_match_uri (httpMessage, NODE_API_URI)) {
			processCallToNodeAPI (connection, httpMessage);
		} else if (mg_http_match_uri (httpMessage, ROUTE_API_URI)) {
			processCallToRouteAPI (connection, httpMessage);
		} else {
			struct mg_http_serve_opts opts = {.root_dir = ROOT_DIR};
			mg_http_serve_dir(connection, httpMessage, &opts);
		}
	}
}

ServerStatus startPathfinderServer (const PortNumber socket) {
	if (!isFullyInitialised ()) {
		return UNKNOWN_ERROR;
	}
	snprintf (httpAddress + strlen (httpAddress), 40 - strlen (httpAddress), "%d", socket);
	struct mg_mgr httpServerManager;

	mg_log_set (MG_LL_DEBUG);
	mg_mgr_init(&httpServerManager);

	if (mg_http_listen(&httpServerManager, httpAddress, httpEventHandler, NULL)) {
		for (;;) mg_mgr_poll(&httpServerManager, 1000);
		mg_mgr_free(&httpServerManager);
		return SUCCESSFULLY_TERMINATED;
	} else {
		mg_mgr_free(&httpServerManager);
		return SOCKET_UNAVAILABLE;
	}
}