#include "../pathfinder.h"

static bool graphLoaded = false;
static bool initialisationComplete = false;
static bool routeReady = false;

static Graph *graph;
static QuadTree *quadTree;

Coordinate coordinateOf (const double latitude, const double longitude) {
    Coordinate coordinate;
    coordinate.latitude = latitude;
    coordinate.longitude = longitude;
    return coordinate;
}

bool loadGraph(const char *const graphFile)  {
    graphLoaded = false;
    graph = loadGraphFromFile (graphFile);
    if (graph != NULL) {
        graphLoaded = initializeDijkstra (graph);
    }
    return graphLoaded;
}

void unloadGraph (void) {
    destroyDijkstra ();
    //unloadQuadTree (quadTree);
    free (graph->nodes);
    free (graph->edges);
    free (graph->mapNodes);
    graph = NULL;
    quadTree = NULL;
    graphLoaded = false;
    initialisationComplete = false;
    routeReady = false;
}

bool setupClosestNodeDataStructure (void) {
    assert (graphLoaded);
    quadTree = loadQuadTree(graph);
    initialisationComplete = quadTree != NULL;
    return initialisationComplete;
}

NodeID calculateRoute(const Coordinate source, const Coordinate destination) {
    assert (isFullyInitialised());
    const NodeID destinationNode = getClosestNode (destination);
    doDijkstra (*graph, getClosestNode (source), destinationNode, ONE_TO_ONE);
    return destinationNode;
}

void doOneToAllDijkstra(const NodeID source) {
    assert (isFullyInitialised() && source <= getLargestNodeID());
    doDijkstra (*graph, source, 0u, ONE_TO_ALL);
    routeReady = true;
}

bool doPointToPointDijkstra (const NodeID source, const NodeID destination) {
    assert (isFullyInitialised());
    const bool routeFound = doDijkstra (*graph, source, destination, ONE_TO_ONE);
    routeReady = true;
    return routeFound;
}

Distance getDistance(const NodeID destination) {
    assert (routeReady);
    return graph->mapNodes[destination].distance;
}

Coordinate getClosestNodeCoordinate(const Coordinate point) {
    assert (isFullyInitialised());
    return graph->mapNodes[getClosestNode (point)].coordinate;
}

NodeID getClosestNode (const Coordinate point) {
    assert (isFullyInitialised());
    QTNode *closest = closestNode(point, quadTree);
    return closest->nodeID; 
}

NodeID getLargestNodeID (void) {
    assert (isFullyInitialised());
    return graph->numberOfNodes - 1u;
}

bool isFullyInitialised (void) {
    return initialisationComplete;
}

Graph getGraph (void) {
    assert (isFullyInitialised());
    return *graph;
}